<?php
	$setting = mysql_fetch_array(mysql_query("SELECT * FROM setting WHERE id_setting='1'"));
	$servername = $setting['db_host'];
	$username = $setting['db_user'];
	$password = $setting['db_pass'];
	$dbname = $setting['db_name'];
	$serverPusat = $setting['id_server'];
	
	function download_file($url, $path)
	{
		$newfilename = $path;
		$file = fopen($url, "rb");
		if ($file) {
			$newfile = fopen($newfilename, "wb");
			
			if ($newfile)
				while (!feof($file)) {
					fwrite($newfile, fread($file, 1024 * 8), 1024 * 8);
				}
		}
		
		if ($file) {
			fclose($file);
		}
		if ($newfile) {
			fclose($newfile);
		}
	}
	
	echo "
        <div class='row'>
            <div class='col-md-12'>
            <div class='box box-primary'>
                <div class='box-header with-border bg-blue'>
                    <h3 class='box-title'>Status Download</h3>
                    <div class='box-tools btn-group'>
						<a href='?pg=sycn&ac=startsycn' class='btn btn-sm btn-danger'><i class='fa fa-download'> Start Sycn </i></a>
                    </div>
                </div><!-- /.box-header -->
                
                <div class='box-body'>
                    <ul class='list-group'>
                        <li class='list-group-item'>DATA 1</li>
                        <li class='list-group-item'>
                        	<div id='progress_data1' style='width:100%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px'></div>
                        	<div id='information_data1' style='width:25%;'></div></li>
                    </ul>
                    
                    <ul class='list-group'>
                        <li class='list-group-item'>DATA 2</li>
                        <li class='list-group-item'>
                        	<div id='progress_data2' style='width:100%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px'></div>
                        	<div id='information_data2' style='width:25%;'></div></li>
                    </ul>
                    
                    <ul class='list-group'>
                        <li class='list-group-item'>DATA 3</li>
                        <li class='list-group-item'>
                        	<div id='progress_data3' style='width:100%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px'></div>
                        	<div id='information_data3' style='width:25%;'></div></li>
                    </ul>
                    
                    <ul class='list-group'>
                        <li class='list-group-item'>DATA 4</li>
                        <li class='list-group-item'>
                        	<div id='progress_data4' style='width:100%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px'></div>
                        	<div id='information_data4' style='width:25%;'></div></li>
                    </ul>
                    
                    <ul class='list-group'>
                        <li class='list-group-item'>DATA 5</li>
                        <li class='list-group-item'>
                        	<div id='progress_data5' style='width:100%; border:1px solid #ccc; padding:5px; margin-top:10px; height:33px'></div>
                        	<div id='information_data5' style='width:25%;'></div></li>
                    </ul>
                </div>
            </div>
        </div>
		
		";
	
	if ($ac == 'startsycn') {
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		/// SYCN DATA SISWA
		truncate('siswa');
		truncate('mapel');
		truncate('soal');
		truncate('nilai');
		truncate('log');
		truncate('login');
		truncate('hasil_jawaban');
		truncate('kelas');
		
		$sqlDataSiswa = "SELECT * FROM siswa";
		
		$resultDataSiswa = $conn->query($sqlDataSiswa);
		$i = 1;
		$baris = $resultDataSiswa->num_rows;
		if ($resultDataSiswa->num_rows > 0) {
			mysql_query('TRUNCATE TABLE siswa');
			// output data of each row
			while ($row = $resultDataSiswa->fetch_assoc()) {
				$insert = mysql_query("INSERT INTO siswa (id_siswa,id_kelas,nis,no_peserta,nama,ruang,sesi,username,password,foto)
											 values ('$row[id_siswa]','$row[id_kelas]','$row[nis]','$row[no_peserta]','$row[nama]','$row[ruang]','$row[sesi]','$row[username]','$row[password]','$row[foto]')");

//				$foto = "$serverPusat/foto/fotosiswa/" . $row["foto"];
//				if (getimagesize($foto)) {
//					copy($foto, "../foto/fotosiswa/" . $row["foto"]);
//				}
				
				if ($insert) {
					$percent = intval($i / $baris * 100) . "%";
					// Javascript for updating the progress bar and information
					echo '<script language="javascript">
				document.getElementById("progress_data1").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data1").innerHTML="  Download Berkas  <b>' . $i . '</b> row(s) of <b>' . $baris . '</b> ... ' . $percent . '  Completed.";
				</script>';
					// This is for the buffer achieve the minimum size in order to flush data
					echo str_repeat(' ', 1024 * 64);
					// Send output to browser immediately
					flush();
					// Tell user that the process is completed
					$i++;
				} else {
					echo mysql_error();
				}
				
			}
			
		} else {
			print "0 results data siswa";
		}
		
		/// SYCN DATA KELAS
		$sqlDataRombel = "SELECT * FROM kelas ";
		
		$resultDataRombel = $conn->query($sqlDataRombel);
		$i = 1;
		$baris = $resultDataRombel->num_rows;
		if ($resultDataRombel->num_rows > 0) {
			mysql_query("TRUNCATE TABLE kelas");
			// output data of each row
			while ($row = $resultDataRombel->fetch_assoc()) {
				$insert = mysql_query("INSERT INTO kelas (id_kelas,level,nama)
								values ('$row[id_kelas]','$row[level]','$row[nama]')");
				
				if ($insert) {
					$percent = intval($i / $baris * 100) . "%";
					// Javascript for updating the progress bar and information
					echo '<script>
				document.getElementById("progress_data2").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data2").innerHTML="  Download Berkas  <b>' . $i . '</b> row(s) of <b>' . $baris . '</b> ... ' . $percent . '  Completed.";
				</script>';
					// This is for the buffer achieve the minimum size in order to flush data
					echo str_repeat(' ', 1024 * 64);
					// Send output to browser immediately
					flush();
					// Tell user that the process is completed
					$i++;
				} else {
					echo mysql_error();
				}
			}
			
		} else {
			echo "0 results";
		}
		
		/// SYCN DATA UJIAN///
		truncate('ujian');
		$sqlDataUjian = "SELECT * FROM ujian WHERE status = 1";
		
		$resultDataUjian = $conn->query($sqlDataUjian);
		$j = 1;
		$barisUjian = $resultDataUjian->num_rows;
		if ($resultDataUjian->num_rows > 0) {
			// output data of each row
			while ($rowUjian = $resultDataUjian->fetch_assoc()) {
				$insert = mysql_query("INSERT INTO ujian (kode_ujian,id_ujian,id_guru,id_mapel,nama,jml_soal,jml_esai,bobot_pg,bobot_esai,tampil_pg,tampil_esai,lama_ujian,tgl_ujian,tgl_selesai,waktu_ujian,selesai_ujian,kelas,acak,token,status,hasil)
								values ('$rowUjian[kode_ujian]','$rowUjian[id_ujian]','$rowUjian[id_guru]','$rowUjian[id_mapel]','$rowUjian[nama]','$rowUjian[jml_soal]','$rowUjian[jml_esai]','$rowUjian[bobot_pg]','$rowUjian[bobot_esai]','$rowUjian[tampil_pg]','$rowUjian[tampil_esai]','$rowUjian[lama_ujian]','$rowUjian[tgl_ujian]','$rowUjian[tgl_selesai]','$rowUjian[waktu_ujian]','$rowUjian[selesai_ujian]','$rowUjian[kelas]','$rowUjian[acak]','$rowUjian[token]','1','$rowUjian[hasil]')");
				
				if ($insert) {
					$percent = intval($j / $barisUjian * 100) . "%";
					// Javascript for updating the progress bar and information
					echo '<script>
				document.getElementById("progress_data3").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data3").innerHTML="  Download Berkas  <b>' . $j . '</b> row(s) of <b>' . $barisUjian . '</b> ... ' . $percent . '  Completed.";
				</script>';
					// This is for the buffer achieve the minimum size in order to flush data
					echo str_repeat(' ', 1024 * 64);
					// Send output to browser immediately
					flush();
					// Tell user that the process is completed
					$j++;
					
					/// SYCN DATA BANKSOALUJAIN  ///
					
					$sqlDataBankSoal = "SELECT*FROM mapel WHERE id_mapel=$rowUjian[id_mapel]";
					$resultDataBankSoal = $conn->query($sqlDataBankSoal);
					$i = 1;
					$baris = $resultDataBankSoal->num_rows;
					if ($resultDataBankSoal->num_rows > 0) {
						
						// output data of each row
						while ($row = $resultDataBankSoal->fetch_assoc()) {
							$insert = mysql_query("INSERT INTO mapel (id_mapel,idguru,nama,jml_soal,jml_esai,tampil_pg,tampil_esai,bobot_pg,bobot_esai,kelas,date,status,statusujian)
								values ( '$row[id_mapel]','$row[idguru]','$row[nama]','$row[jml_soal]','$row[jml_esai]','$row[tampil_pg]','$row[tampil_esai]','$row[bobot_pg]','$row[bobot_esai]','$row[kelas]', '$row[date]','$row[status]','$row[statusujian]')");
							
							if ($insert) {
								$percent = intval($i / $baris * 100) . "%";
								// Javascript for updating the progress bar and information
								echo '<script language="javascript">
				document.getElementById("progress_data4").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data4").innerHTML="  Download Berkas  <b>' . $i . '</b> row(s) of <b>' . $baris . '</b> ... ' . $percent . '  Completed.";
				</script>';
								// This is for the buffer achieve the minimum size in order to flush data
								echo str_repeat(' ', 1024 * 64);
								// Send output to browser immediately
								flush();
								// Tell user that the process is completed
								$i++;
							} else {
								echo mysql_error();
							}
							
							$sqlDataSoal = "SELECT * FROM soal WHERE id_mapel = $row[id_mapel]";
							
							$resultDataSoal = $conn->query($sqlDataSoal);
							$ii = 1;
							$barisSoal = $resultDataSoal->num_rows;
							if ($resultDataSoal->num_rows > 0) {
								// output data of each row
								while ($rowSoal = $resultDataSoal->fetch_assoc()) {
									$insertSoal = mysql_query("INSERT INTO soal (id_soal,id_mapel,nomor,soal,jenis,pilA,pilB,pilC,pilD,pilE,jawaban,file,file1,fileA,fileB,fileC,fileD,fileE)
											values ('$rowSoal[id_soal]','$rowSoal[id_mapel]',	'$rowSoal[nomor]','$rowSoal[soal]','$rowSoal[jenis]','$rowSoal[pilA]','$rowSoal[pilB]','$rowSoal[pilC]','$rowSoal[pilD]','$rowSoal[pilE]','$rowSoal[jawaban]','$rowSoal[file]','$rowSoal[file1]','$rowSoal[fileA]','$rowSoal[fileB]','$rowSoal[fileC]','$rowSoal[fileD]','$rowSoal[fileE]')");
									
									
									if ($rowSoal["file"]) {
										$attachSoal1 = "$serverPusat/files/" . $rowSoal["file"];
										if (getimagesize($attachSoal1)) {
											copy($attachSoal1, "../files/" . $rowSoal["file"]);
										}
									}
									if ($rowSoal["file1"]) {
										$attachSoal2 = "$serverPusat/files/" . $rowSoal["file1"];
										if (getimagesize($attachSoal2)) {
											copy($attachSoal2, "../files/" . $rowSoal["file1"]);
										}
									}
									if ($rowSoal["fileA"]) {
										$attachSoalA = "$serverPusat/files/" . $rowSoal["fileA"];
										if (getimagesize($attachSoalA)) {
											copy($attachSoalA, "../files/" . $rowSoal["fileA"]);
										}
									}
									if ($rowSoal["fileB"]) {
										$attachSoalB = "$serverPusat/files/" . $rowSoal["fileB"];
										if (getimagesize($attachSoalB)) {
											copy($attachSoalB, "../files/" . $rowSoal["fileB"]);
										}
									}
									if ($rowSoal["fileC"]) {
										$attachSoalC = "$serverPusat/files/" . $rowSoal["fileC"];
										if (getimagesize($attachSoalC)) {
											copy($attachSoalC, "../files/" . $rowSoal["fileC"]);
										}
									}
									if ($rowSoal["fileD"]) {
										$attachSoalD = "$serverPusat/files/" . $rowSoal["fileD"];
										if (getimagesize($attachSoalD)) {
											copy($attachSoalD, "../files/" . $rowSoal["fileD"]);
										}
									}
									if ($rowSoal["fileE"]) {
										$attachSoalE = "$serverPusat/files/" . $rowSoal["fileE"];
										if (getimagesize($attachSoalE)) {
											copy($attachSoalE, "../files/" . $rowSoal["fileE"]);
										}
									}
									
									
									if ($insertSoal) {
										$percent = intval($ii / $barisSoal * 100) . "%";
										echo '<script language="javascript">
							document.getElementById("progress_data5").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
							document.getElementById("information_data5").innerHTML="  Download Berkas  <b>' . $ii . '</b> row(s) of <b>' . $barisSoal . '</b> ... ' . $percent . '  Completed.";
							</script>';
										// This is for the buffer achieve the minimum size in order to flush data
										echo str_repeat(' ', 1024 * 64);
										// Send output to browser immediately
										flush();
										// Tell user that the process is completed
										$ii++;
									} else {
										echo mysql_error();
									}
									
									
								}
								
							} else {
								echo "0 results soal";
							}
							
						}
						
					} else {
						echo "0 results bank soal";
					}
					
				} else {
					echo mysql_error();
				}
			}
			
		} else {
			echo "0 results ujian";
		}
		
		
		$conn->close();
		echo "<script>swal ( 'OK',  'Proses upload telah selesai!' ,  'success' ) </script>";
		
		
	} elseif ($ac == 'startupload') {
		
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		// UploadNilai
		$nilaiq = mysql_query("SELECT * FROM nilai WHERE upload = 0 AND total > 0");
		$i = 1;
		$baris = mysql_num_rows($nilaiq);
		
		if (mysql_num_rows($nilaiq) > 0) {
			while ($nilai = mysql_fetch_array($nilaiq)) {
				$sqlUploadNilai =
					"INSERT INTO nilai (
                   jawaban,
                   id_ujian,
                   kode_ujian,
                   id_mapel,
                   id_siswa,
                   ujian_mulai,
                   ujian_berlangsung,
                   ujian_selesai,
                   jml_benar,
                   jml_salah,
                   nilai_esai,
                   skor,
                   total,
                   ipaddress,
                   hasil,
                   status)
					values (
					        '$nilai[jawaban]',
					        '$nilai[id_ujian]',
					        '$nilai[kode_ujian]',
					        '$nilai[id_mapel]',
					        '$nilai[id_siswa]',
					        '$nilai[ujian_mulai]',
					        '$nilai[ujian_berlangsung]',
					        '$nilai[ujian_selesai]',
					        '$nilai[jml_benar]',
					        '$nilai[jml_salah]',
					        '$nilai[nilai_esai]',
					        '$nilai[skor]',
					        '$nilai[total]',
					        '$nilai[ipaddress]',
					        '$nilai[hasil]',
					        1)";
				
				$resultUploadNilai = $conn->query($sqlUploadNilai);
				
				if ($resultUploadNilai) {
					mysql_query("UPDATE nilai SET upload = 1 WHERE id_nilai = $nilai[id_nilai]");
					$percent = intval($i / $baris * 100) . "%";
					// Javascript for updating the progress bar and information
					echo '<script language="javascript">
				document.getElementById("progress_data1").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data1").innerHTML="  Upload Berkas  <b>' . $i . '</b> row(s) of <b>' . $baris . '</b> ... ' . $percent . '  Completed.";
				</script>';
					// This is for the buffer achieve the minimum size in order to flush data
					echo str_repeat(' ', 1024 * 64);
					// Send output to browser immediately
					flush();
					// Tell user that the process is completed
					$i++;
				} else {
					echo mysqli_error($conn);
				}
			}
			
		} else {
			print " 0 data for upload ";
		}
		
		
		// UploadJawaban
		$jawabanq = mysql_query("SELECT * FROM hasil_jawaban WHERE upload = 0");
		$i = 1;
		$baris = mysql_num_rows($jawabanq);
		if (mysql_num_rows($jawabanq) > 0) {
			while ($jawab = mysql_fetch_array($jawabanq)) {
				$sqlUploadJawaban = "INSERT INTO jawaban (
                     id_siswa,
                     id_ujian,
					 id_mapel,
                     id_soal,
                     jawaban,
                     jenis,
                     esai,
                     nilai_esai,
                     ragu) values (
                                   '$jawab[id_siswa]',
                                   '$jawab[id_ujian]',
                                   '$jawab[id_mapel]',
                                   '$jawab[id_soal]',
                                   '$jawab[jawaban]',
                                   '$jawab[jenis]',
                                   '$jawab[esai]',
                                   '$jawab[nilai_esai]',
                                   '$jawab[ragu]')";
				
				$resultUploadJawaban = $conn->query($sqlUploadJawaban);
				if ($resultUploadJawaban) {
					mysql_query("UPDATE hasil_jawaban SET upload = 1 WHERE id_jawaban = $jawab[id_jawaban]");
					$percent = intval($i / $baris * 100) . "%";
					// Javascript for updating the progress bar and information
					echo '<script>
				document.getElementById("progress_data2").innerHTML="<div style=\"width:' . $percent . ';background-image:url(../foto/loading.gif);\">&nbsp;</div>";
				document.getElementById("information_data2").innerHTML="  Upload Berkas  <b>' . $i . '</b> row(s) of <b>' . $baris . '</b> ... ' . $percent . '  Completed.";
				</script>';
					// This is for the buffer achieve the minimum size in order to flush data
					echo str_repeat(' ', 1024 * 64);
					// Send output to browser immediately
					flush();
					// Tell user that the process is completed
					$i++;
					
				} else {
					echo mysqli_error($conn);
				}
				
			}
			
		} else {
			echo " 0 data for upload jawaban";
		}
		$conn->close();
	}
	
	