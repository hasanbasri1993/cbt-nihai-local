/*
 Navicat MySQL Data Transfer

 Source Server         : w
 Source Server Type    : MySQL
 Source Server Version : 50643
 Source Host           : localhost:3306
 Source Schema         : cbtruang

 Target Server Type    : MySQL
 Target Server Version : 50643
 File Encoding         : 65001

 Date: 08/04/2019 15:07:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hasil_jawaban
-- ----------------------------
DROP TABLE IF EXISTS `hasil_jawaban`;
CREATE TABLE `hasil_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL,
  `upload` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for jawaban
-- ----------------------------
DROP TABLE IF EXISTS `jawaban`;
CREATE TABLE `jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text NOT NULL,
  `nilai_esai` int(5) NOT NULL,
  `ragu` int(1) NOT NULL,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `id_kelas` varchar(11) NOT NULL,
  `level` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas
-- ----------------------------
BEGIN;
INSERT INTO `kelas` VALUES ('291', '6', '6A');
INSERT INTO `kelas` VALUES ('325', 'XII', 'XIIA MIA');
INSERT INTO `kelas` VALUES ('326', 'XII', 'XIIB MIA');
INSERT INTO `kelas` VALUES ('327', 'XII', 'XIIC MIA');
INSERT INTO `kelas` VALUES ('328', 'XII', 'XIID MIA');
INSERT INTO `kelas` VALUES ('329', 'XII', 'XIIE IIS');
INSERT INTO `kelas` VALUES ('330', 'XII', 'XIIF IIS');
INSERT INTO `kelas` VALUES ('331', 'XII', 'XIIG IIS');
INSERT INTO `kelas` VALUES ('332', 'XII', 'XIIH IIS');
COMMIT;

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `kode_level` varchar(5) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `text` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for mapel
-- ----------------------------
DROP TABLE IF EXISTS `mapel`;
CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL AUTO_INCREMENT,
  `idpk` varchar(10) DEFAULT NULL,
  `idguru` varchar(3) NOT NULL,
  `id_semester` int(2) NOT NULL,
  `nama` varchar(100) CHARACTER SET utf32 COLLATE utf32_bin NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `level` varchar(5) DEFAULT NULL,
  `kelas` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(2) NOT NULL,
  `statusujian` int(11) NOT NULL,
  PRIMARY KEY (`id_mapel`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mapel
-- ----------------------------
BEGIN;
INSERT INTO `mapel` VALUES (1, NULL, '51', 8, 'Tahriri Niahie 2019 - Tajwid', 50, 0, 25, 0, 100, 0, NULL, 'a:9:{i:0;s:3:\"291\";i:1;s:3:\"325\";i:2;s:3:\"326\";i:3;s:3:\"327\";i:4;s:3:\"328\";i:5;s:3:\"329\";i:6;s:3:\"330\";i:7;s:3:\"331\";i:8;s:3:\"332\";}', '2019-04-08 11:44:45', '1', 0);
COMMIT;

-- ----------------------------
-- Table structure for mata_pelajaran
-- ----------------------------
DROP TABLE IF EXISTS `mata_pelajaran`;
CREATE TABLE `mata_pelajaran` (
  `kode_mapel` varchar(20) NOT NULL,
  `nama_mapel` varchar(50) CHARACTER SET utf32 COLLATE utf32_bin NOT NULL,
  `bobot_mapel` int(1) DEFAULT NULL,
  `paragrap_mapel` enum('kiri','kanan') DEFAULT 'kanan',
  PRIMARY KEY (`kode_mapel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for nilai
-- ----------------------------
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) NOT NULL,
  `ujian_selesai` varchar(20) NOT NULL,
  `jml_benar` int(10) NOT NULL,
  `jml_salah` int(10) NOT NULL,
  `nilai_esai` varchar(10) NOT NULL,
  `skor` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `hasil` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  `upload` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pengacak
-- ----------------------------
DROP TABLE IF EXISTS `pengacak`;
CREATE TABLE `pengacak` (
  `id_pengacak` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` varchar(255) NOT NULL,
  `id_esai` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pengacak`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengacak
-- ----------------------------
BEGIN;
INSERT INTO `pengacak` VALUES (30, 2922, 16, '400,399,398,397,396,395,394,393,392,391,390,389,388,387,386,385,384,383,382,381,380,379,378,377,376,375,374,373,372,371,370,369,368,367,366,365,364,363,362,361,', '');
INSERT INTO `pengacak` VALUES (42, 2922, 16, '454,455,452,496,459,472,485,473,462,471,489,486,497,498,491,493,453,476,466,456,461,483,488,467,469,501,500,499,494,478,470,468,465,477,458,481,463,457,479,492,475,474,487,495,480,484,482,464,460,490,', '');
INSERT INTO `pengacak` VALUES (43, 3021, 20, '1303,1284,1281,1285,1296,1301,1283,1290,1299,1272,1273,1276,1291,1306,1277,1297,1266,1271,1286,1307,1293,1295,1294,1287,1288,1289,1304,1262,1292,1263,1268,1270,1267,1274,1305,1308,1302,1278,1275,1265,1264,1282,1298,1279,1280,1269,1309,1300,1310,1311,', '');
INSERT INTO `pengacak` VALUES (51, 2926, 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,', '');
COMMIT;

-- ----------------------------
-- Table structure for pengawas
-- ----------------------------
DROP TABLE IF EXISTS `pengawas`;
CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(10) NOT NULL,
  `no_ktp` varchar(16) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat_jalan` varchar(255) NOT NULL,
  `rt_rw` varchar(8) NOT NULL,
  `dusun` varchar(50) NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kode_pos` int(6) NOT NULL,
  `nuptk` varchar(20) NOT NULL,
  `bidang_studi` varchar(50) NOT NULL,
  `jenis_ptk` varchar(50) NOT NULL,
  `tgs_tambahan` varchar(50) NOT NULL,
  `status_pegawai` varchar(50) NOT NULL,
  `status_aktif` varchar(20) NOT NULL,
  `status_nikah` varchar(20) NOT NULL,
  `sumber_gaji` varchar(30) NOT NULL,
  `ahli_lab` varchar(10) NOT NULL,
  `nama_ibu` varchar(40) NOT NULL,
  `nama_suami` varchar(50) NOT NULL,
  `nik_suami` varchar(20) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL,
  `tmt` date NOT NULL,
  `keahlian_isyarat` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(10) NOT NULL,
  `npwp` varchar(16) NOT NULL,
  `foto` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pengawas`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengawas
-- ----------------------------
BEGIN;
INSERT INTO `pengawas` VALUES (9, '-', 'administrator', '', 'admin', '$2y$10$3fVC8VJfm8ElEv6PNLT2R.XalOF.sFq7TOgJE54p5KQm2oL/0N1Im', 'admin', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (41, '-', 'Pajar Sidik Nurfakhri', '', 'pajarsidikn', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (42, '-', 'Guru 2', '', 'guru2', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (43, '-', 'Guru 3', '', 'guru3', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (44, '-', 'Guru 4', '', 'guru4', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (45, '-', 'Guru 5', '', 'guru5', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (46, '-', 'Guru 6', '', 'guru6', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (47, '-', 'Guru 7', '', 'guru7', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (48, '-', 'Guru 8', '', 'guru8', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (49, '-', 'Guru 9', '', 'guru9', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (50, '-', 'Ruli Syahruli, S.Pd', '', 'ruli', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
INSERT INTO `pengawas` VALUES (51, '-', 'Agus Prasetyo, S.Pd', '', 'agus', '123456', 'guru', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman` (
  `id_pengumuman` int(5) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `user` int(3) NOT NULL,
  `text` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pengumuman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pk
-- ----------------------------
DROP TABLE IF EXISTS `pk`;
CREATE TABLE `pk` (
  `id_pk` varchar(10) NOT NULL,
  `program_keahlian` varchar(50) NOT NULL,
  `kode` varchar(10) NOT NULL,
  PRIMARY KEY (`id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ruang
-- ----------------------------
DROP TABLE IF EXISTS `ruang`;
CREATE TABLE `ruang` (
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `npsn` text NOT NULL,
  `ns` varchar(50) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for semester
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` enum('Ganjil','Genap') NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `tahunajaran` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of semester
-- ----------------------------
BEGIN;
INSERT INTO `semester` VALUES (1, 'Ganjil', 'N', '');
INSERT INTO `semester` VALUES (2, 'Genap', 'N', '');
INSERT INTO `semester` VALUES (3, 'Ganjil', 'N', '2016-2017');
INSERT INTO `semester` VALUES (4, 'Genap', 'N', '2016-2017');
INSERT INTO `semester` VALUES (5, 'Ganjil', 'N', '2017-2018');
INSERT INTO `semester` VALUES (6, 'Genap', 'N', '2017-2018');
INSERT INTO `semester` VALUES (7, 'Ganjil', 'N', '2018 - 2019');
INSERT INTO `semester` VALUES (8, 'Genap', 'Y', '2018 - 2019');
INSERT INTO `semester` VALUES (9, 'Ganjil', 'N', '2019 - 2020');
INSERT INTO `semester` VALUES (10, 'Genap', 'N', '2019 - 2020');
INSERT INTO `semester` VALUES (11, 'Ganjil', 'N', '2020 - 2021');
INSERT INTO `semester` VALUES (12, 'Genap', 'N', '2020 - 2021');
INSERT INTO `semester` VALUES (13, 'Ganjil', 'N', '');
INSERT INTO `semester` VALUES (14, 'Genap', 'N', '');
INSERT INTO `semester` VALUES (15, 'Ganjil', 'N', '');
INSERT INTO `semester` VALUES (16, 'Genap', 'N', '');
COMMIT;

-- ----------------------------
-- Table structure for server
-- ----------------------------
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `kode_server` varchar(20) NOT NULL,
  `nama_server` varchar(30) NOT NULL,
  `status` enum('aktif','tidak') NOT NULL DEFAULT 'tidak',
  `status_sycn` enum('sudah','belum') NOT NULL DEFAULT 'belum',
  PRIMARY KEY (`kode_server`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of server
-- ----------------------------
BEGIN;
INSERT INTO `server` VALUES ('LAB_MA', 'LAB MA', 'tidak', 'belum');
INSERT INTO `server` VALUES ('LAB_MTA', 'LAB TAHFIDZH', 'tidak', 'belum');
INSERT INTO `server` VALUES ('LAB_MTS', 'LAB MTs', 'tidak', 'belum');
INSERT INTO `server` VALUES ('LAB_SMP', 'LAB SMP', 'tidak', 'belum');
COMMIT;

-- ----------------------------
-- Table structure for sesi
-- ----------------------------
DROP TABLE IF EXISTS `sesi`;
CREATE TABLE `sesi` (
  `kode_sesi` varchar(10) NOT NULL,
  `nama_sesi` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_sesi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_time` varchar(10) NOT NULL,
  `session_hash` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi` varchar(100) NOT NULL,
  `sekolah` varchar(50) NOT NULL,
  `jenjang` varchar(5) NOT NULL,
  `kepsek` varchar(50) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `web` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `header` text NOT NULL,
  `header_kartu` text NOT NULL,
  `serverlocal_kode` varchar(10) NOT NULL,
  `serverpusat_host` varchar(100) NOT NULL,
  `serverpusat_hostdb` varchar(255) DEFAULT NULL,
  `serverpusat_userdb` varchar(100) DEFAULT NULL,
  `serverpusat_passworddb` varchar(100) DEFAULT NULL,
  `serverpusat_db` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setting
-- ----------------------------
BEGIN;
INSERT INTO `setting` VALUES (1, ' Ujian Kelas Akhir Computer Based Test', 'MA Daarul Uluum Lido', 'MA', 'Azhari Muchtar, S.Ag', '-', 'JL Mayjen HR Edi Sukma KM 22 Muara Ciburuy', 'Cigombong', 'Bogor', '0251 8224572', '', 'https://daarululuumlido.com/', 'sekretariat@daarululuumlido.com', 'dist/img/logo39.png', 'YAYASAN SALSABILA LIDO', 'KARTU PESERTA\nUJIAN SEKOLAH BERBASIS KOMPUTER', 'lab_mts', 'http://localhost:8080/candycbt', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` varchar(11) DEFAULT NULL,
  `idpk` varchar(10) DEFAULT NULL,
  `nis` varchar(30) DEFAULT NULL,
  `no_peserta` varchar(30) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `level` varchar(5) DEFAULT NULL,
  `ruang` varchar(10) DEFAULT NULL,
  `sesi` int(2) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  `foto` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa
-- ----------------------------
BEGIN;
INSERT INTO `siswa` VALUES (1065, '291', NULL, '136228', '136228', 'Muhammad Rifki Syahrul Fauzi', NULL, 'lab_ma', 1, '136228', 'oamxhk8', '9f023de257b645deed6bc0cf3721db59.JPG', NULL);
INSERT INTO `siswa` VALUES (1070, '291', NULL, '136229', '136229', 'Saufan', NULL, 'lab_ma', 1, '136229', '0i5egvc', '66de1db1b4fb9d291b3f09c5d49592a0.JPG', NULL);
INSERT INTO `siswa` VALUES (1839, '291', NULL, '136237', '136237', 'Ahmad Syabani Fadhli', NULL, 'lab_ma', 1, '136237', '6y7zpfg', 'a4abc50c09d1d5dccf3024170073c385.JPG', NULL);
INSERT INTO `siswa` VALUES (1842, '291', NULL, '136223', '136223', 'Khodam Fatih Ahmad', NULL, 'lab_ma', 1, '136223', '20ldra3', 'b828ec32168a909e812115581a34f59b.JPG', NULL);
INSERT INTO `siswa` VALUES (1873, '291', NULL, '136222', '136222', 'Annafi Supiah', NULL, 'lab_ma', 1, '136222', 'w3vlgp5', '16f157b6619725a9db0ff8352e25d011.jpg', NULL);
INSERT INTO `siswa` VALUES (1884, '291', NULL, '136234', '136234', 'Lulu Huluwiah Awalani', NULL, 'lab_ma', 1, '136234', 'b8s483g', '032fdaa4d8a48aa776cd7f0653db282d.jpg', NULL);
INSERT INTO `siswa` VALUES (1886, '291', NULL, '136235', '136235', 'Nabilatul Laila', NULL, 'lab_ma', 1, '136235', 'nh2i1no', 'e6da88315a62669b117bb2a9bcd52de0.jpg', NULL);
INSERT INTO `siswa` VALUES (1890, '291', NULL, '136236', '136236', 'Nurlaela Sofiatuzahra', NULL, 'lab_ma', 1, '136236', 'h8devc8', '54300ff1d890a8722cc7e677bc547ffb.jpg', NULL);
INSERT INTO `siswa` VALUES (1988, '291', NULL, '136230', '136230', 'Siti Zahra', NULL, 'lab_ma', 1, '136230', 'ps1ei6u', 'af8ed03a4a35962185fdca899e74826d.jpg', NULL);
INSERT INTO `siswa` VALUES (2026, '325', NULL, '154001', '154001', 'Abdul Farhan ', NULL, 'lab_ma', 1, '154001', 'f0ast', 'eeaee5240092754e80a7f851d9b45138.jpg', NULL);
INSERT INTO `siswa` VALUES (2030, '325', NULL, '154015', '154015', 'Daniel Isya Abdullah', NULL, 'lab_ma', 1, '154015', 'j3565', '0c83a2be1fad780964565e1e3fab1e6f.jpg', NULL);
INSERT INTO `siswa` VALUES (2032, '325', NULL, '154027', '154027', 'Fadel Muhammad ', NULL, 'lab_ma', 1, '154027', 'cgfzz', '019deeb4029b7e4bfdc11baa1eb6368f.jpg', NULL);
INSERT INTO `siswa` VALUES (2033, '331', NULL, '154037', '154037', 'Hendra Robby Muhammad Awaludin', NULL, 'lab_ma', 1, '154037', '23l6t', '', NULL);
INSERT INTO `siswa` VALUES (2034, '329', NULL, '154039', '154039', 'Hilman Maulana (i)', NULL, 'lab_ma', 1, '154039', 'x2ct8', '86cc7deddfeb4063f494b73f5ee08ea5.jpg', NULL);
INSERT INTO `siswa` VALUES (2035, '331', NULL, '154043', '154043', 'Iriansyah Anugrah Pradana', NULL, 'lab_ma', 1, '154043', '81x4m', 'effa865c5771ae23627b19c94aead62b.jpg', NULL);
INSERT INTO `siswa` VALUES (2036, '291', NULL, '154044', '154044', 'Irsyad Nawawi Rafsanjani', NULL, 'lab_ma', 1, '154044', '47g70', '0d9bf4676749b2bdf86c4789a5ae8fae.jpg', NULL);
INSERT INTO `siswa` VALUES (2038, '331', NULL, '154053', '154053', 'M. Alby Fauzi', NULL, 'lab_ma', 1, '154053', 'ucd2r', 'c34294f9d1cffb5a9d12160c8011b51a.jpg', NULL);
INSERT INTO `siswa` VALUES (2039, '291', NULL, '154055', '154055', 'Muhammad Ichsan Fatwa Fauzi', NULL, 'lab_ma', 1, '154055', '2sr9d', '9c2a22053d5036fc33c18f50d8dde668.jpg', NULL);
INSERT INTO `siswa` VALUES (2042, '325', NULL, '154063', '154063', 'Muhammad Fiqih Ubaidi', NULL, 'lab_ma', 1, '154063', 'bfbkf', 'fd14404835a0030f634acecfec0f4b5b.jpg', NULL);
INSERT INTO `siswa` VALUES (2044, '329', NULL, '154065', '154065', 'Muhammad Gilang Riandy', NULL, 'lab_ma', 1, '154065', 'uw2tm', '08574dd0b359722a20b84b1de048d9d8.jpg', NULL);
INSERT INTO `siswa` VALUES (2045, '331', NULL, '154066', '154066', 'Muhammad Ikhsan Asari', NULL, 'lab_ma', 1, '154066', '81frr', 'b30916417e67e096dfe4af9f68dc0048.jpg', NULL);
INSERT INTO `siswa` VALUES (2046, '325', NULL, '154067', '154067', 'Muhammad Maulana Firdaus', NULL, 'lab_ma', 1, '154067', 'ms8v9', '248294b3b8098de23a122a7687b5aa0c.jpg', NULL);
INSERT INTO `siswa` VALUES (2047, '329', NULL, '154068', '154068', 'Muhammad Zulkifli', NULL, 'lab_ma', 1, '154068', 'rcsw8', '11a87bc8b3ac04bea0739db903838ad6.jpg', NULL);
INSERT INTO `siswa` VALUES (2049, '331', NULL, '154084', '154084', 'Rizki Amanda', NULL, 'lab_ma', 1, '154084', 'lc89k', '4558497e794b11f0fb740334b2824a8f.jpg', NULL);
INSERT INTO `siswa` VALUES (2050, '291', NULL, '154086', '154086', 'Rizky Saeful Muchtar', NULL, 'lab_ma', 1, '154086', 'j5mfq', 'a603110068feb3edb8b75036816acb85.jpg', NULL);
INSERT INTO `siswa` VALUES (2051, '328', NULL, '154087', '154087', 'Sabilar Rosyad', NULL, 'lab_ma', 1, '154087', 'vl1tn', 'fc09886aaadc9c598b1661a644aab7e9.jpg', NULL);
INSERT INTO `siswa` VALUES (2052, '328', NULL, '154091', '154091', 'Shofil Fuadi Al Munawwar', NULL, 'lab_ma', 1, '154091', 'rfexo', 'd2dec01ed2f779cad8740c1109edc190.jpg', NULL);
INSERT INTO `siswa` VALUES (2053, '291', NULL, '154102', '154102', 'Wahyu Sanjaya', NULL, 'lab_ma', 1, '154102', 'f7mka', '2589295282037d943e28b9261e1fe6b8.jpg', NULL);
INSERT INTO `siswa` VALUES (2058, '325', NULL, '154033', '154033', 'Firan Mayhendra', NULL, 'lab_ma', 1, '154033', 'f7wk6', '', NULL);
INSERT INTO `siswa` VALUES (2060, '291', NULL, '154050', '154050', 'Lucky Daffa Hanif', NULL, 'lab_ma', 1, '154050', 'xyh4s', 'b3d9c5726e8d9aacc98f42c2c6ba1597.jpg', NULL);
INSERT INTO `siswa` VALUES (2061, '329', NULL, '154054', '154054', 'M. Auliya Hakiem', NULL, 'lab_ma', 1, '154054', '3nle4', '11c329203a27d58bd4dc4d56ae21c677.jpg', NULL);
INSERT INTO `siswa` VALUES (2062, '328', NULL, '154056', '154056', 'M. Rizki Fadillah', NULL, 'lab_ma', 1, '154056', 'h7luk', '', NULL);
INSERT INTO `siswa` VALUES (2066, '331', NULL, '154061', '154061', 'Muhammad fachri Romadhon ', NULL, 'lab_ma', 1, '154061', 'xjugo', '', NULL);
INSERT INTO `siswa` VALUES (2067, '331', NULL, '154062', '154062', 'Muhammad Fauzan ', NULL, 'lab_ma', 1, '154062', 'zdi2t', '8be11efe09a82acd0941bf95e033df36.jpg', NULL);
INSERT INTO `siswa` VALUES (2068, '329', NULL, '154083', '154083', 'Raul Maldini', NULL, 'lab_ma', 1, '154083', 'o6yrk', '111b6fa545632c3f3219a788cf04a95d.jpg', NULL);
INSERT INTO `siswa` VALUES (2070, '332', NULL, '154009', '154009', 'Amelia', NULL, 'lab_ma', 1, '154009', 'zud2p', 'b7462de10a7e27b95e8e401b562be5eb.jpg', NULL);
INSERT INTO `siswa` VALUES (2071, '326', NULL, '154014', '154014', 'Ayu Fira Ellena', NULL, 'lab_ma', 1, '154014', '6e7ol', 'c77f5999717a9405bb550a1d47ab9cff.JPG', NULL);
INSERT INTO `siswa` VALUES (2072, '332', NULL, '154016', '154016', 'Dede Nurulgina', NULL, 'lab_ma', 1, '154016', '3zlpe', '83903f6e3a16ddd73d8c79652140202d.jpg', NULL);
INSERT INTO `siswa` VALUES (2074, '330', NULL, '154026', '154026', 'Euis Mawaddaturrahmah', NULL, 'lab_ma', 1, '154026', 'hr44q', 'b985313dddf626fb78c79f095c7bd59b.jpg', NULL);
INSERT INTO `siswa` VALUES (2075, '332', NULL, '154029', '154029', 'Fatimah Rahmah', NULL, 'lab_ma', 1, '154029', 'h2oly', '5fda76dc7cacd582d3b2df0d606973ad.jpg', NULL);
INSERT INTO `siswa` VALUES (2078, '330', NULL, '154075', '154075', 'Nur Intannia', NULL, 'lab_ma', 1, '154075', '5602f', 'f5031b3654524d4b9c5d30e91c0c79fa.jpg', NULL);
INSERT INTO `siswa` VALUES (2081, '291', NULL, '154081', '154081', 'Qisthy Della Hia', NULL, 'lab_ma', 1, '154081', 'onsuz', '932bf2bab517f158a6793151eacb9266.jpg', NULL);
INSERT INTO `siswa` VALUES (2084, '332', NULL, '154096', '154096', 'Sofi Rahmasari', NULL, 'lab_ma', 1, '154096', 'mxkbp', '84eb98634b56a7615f8c10ada025d416.jpg', NULL);
INSERT INTO `siswa` VALUES (2085, '326', NULL, '154007', '154007', 'Alifia Alfatiha Putri Oskandar', NULL, 'lab_ma', 1, '154007', '3gndq', '8b372f20ff25f894754048020ca9913e.jpg', NULL);
INSERT INTO `siswa` VALUES (2087, '330', NULL, '154011', '154011', 'Anita', NULL, 'lab_ma', 1, '154011', '61khj', '6d8a59bca0117f3a7906426f1933552b.jpg', NULL);
INSERT INTO `siswa` VALUES (2088, '332', NULL, '154012', '154012', 'Pindah - Astri Rahmah Sasalmah', NULL, 'lab_ma', 1, '154012', 'f03dg', '3e2506ab7d97f1026074e971140caf35.jpg', NULL);
INSERT INTO `siswa` VALUES (2089, '328', NULL, '154013', '154013', 'Awalliah Noviyanti', NULL, 'lab_ma', 1, '154013', 'z2m99', 'fa381ba7de9910d918c477bf7376ed2f.jpg', NULL);
INSERT INTO `siswa` VALUES (2090, '330', NULL, '154018', '154018', 'Pindah - Desta Valicia', NULL, 'lab_ma', 1, '154018', 'lrep5', 'cbaeae49610720547a5a170e28932601.jpg', NULL);
INSERT INTO `siswa` VALUES (2091, '330', NULL, '154019', '154019', 'Devi Fitria ', NULL, 'lab_ma', 1, '154019', '3j6vy', 'eaf62a3faec3521891c732010076cf2b.jpg', NULL);
INSERT INTO `siswa` VALUES (2092, '332', NULL, '154025', '154025', 'Edenia Rengganis', NULL, 'lab_ma', 1, '154025', '7stoi', '8dc1a3a1b360d86e0f9e9b29a518f97e.jpg', NULL);
INSERT INTO `siswa` VALUES (2093, '327', NULL, '154034', '154034', 'Ghina Rizki Amalia', NULL, 'lab_ma', 1, '154034', 'kwkyc', '82749c8ddc9264ac0ee5f5813af65c87.jpg', NULL);
INSERT INTO `siswa` VALUES (2094, '330', NULL, '154035', '154035', 'Gustina Putri Wulandari', NULL, 'lab_ma', 1, '154035', 'gm8ur', '77a51e6535646a451c66fe946caa8b87.jpg', NULL);
INSERT INTO `siswa` VALUES (2095, '326', NULL, '154041', '154041', 'Ida Sari Mulyati', NULL, 'lab_ma', 1, '154041', '9d3b2', '67cd66f17c6ab5b3567d3d2d1bf3c3cf.jpg', NULL);
INSERT INTO `siswa` VALUES (2096, '326', NULL, '154042', '154042', 'Indah Putri Ramadhanis', NULL, 'lab_ma', 1, '154042', '62kad', '', NULL);
INSERT INTO `siswa` VALUES (2097, '330', NULL, '154045', '154045', 'Pindah - Ismaya Kurnia Sari', NULL, 'lab_ma', 1, '154045', 'e58dh', 'cf0f8a1257c9789d9ce0a92e06268c0e.jpg', NULL);
INSERT INTO `siswa` VALUES (2098, '291', NULL, '154046', '154046', 'Jihan Azzahra', NULL, 'lab_ma', 1, '154046', 'up78n', '10edcf35e448c6b6fb7e41a233aa4e38.jpg', NULL);
INSERT INTO `siswa` VALUES (2099, '327', NULL, '154047', '154047', 'Karina Dewi Nurseptiana', NULL, 'lab_ma', 1, '154047', 'omx29', '62999898dc9835938b0fd26d6a94b94b.jpg', NULL);
INSERT INTO `siswa` VALUES (2100, '332', NULL, '154048', '154048', 'Laelatus Saadah', NULL, 'lab_ma', 1, '154048', 'z0mnm', '', NULL);
INSERT INTO `siswa` VALUES (2101, '330', NULL, '154052', '154052', 'Lyana Herlina', NULL, 'lab_ma', 1, '154052', '0fdai', '5d4443e8fa7aea5190ee1ccc5ab16fd1.jpg', NULL);
INSERT INTO `siswa` VALUES (2102, '330', NULL, '154069', '154069', 'Nabilah Mulki', NULL, 'lab_ma', 1, '154069', 'n9ofl', 'd2eea8753ded84ed37fce1504dc54221.jpg', NULL);
INSERT INTO `siswa` VALUES (2103, '328', NULL, '154072', '154072', 'Neng Sarah Permata Sari', NULL, 'lab_ma', 1, '154072', 'lm7cz', 'a4430c49273b2b529b25855615fbde26.jpg', NULL);
INSERT INTO `siswa` VALUES (2104, '332', NULL, '154079', '154079', 'Putri Maharani Yonanda Fransiska', NULL, 'lab_ma', 1, '154079', 'xzxp8', '', NULL);
INSERT INTO `siswa` VALUES (2106, '330', NULL, '154092', '154092', 'Siti Ahlatil Insaniah Hasibuan ', NULL, 'lab_ma', 1, '154092', 'pnu2c', 'db6cd3fa3ce2afe427b1b0096d6503db.jpg', NULL);
INSERT INTO `siswa` VALUES (2107, '291', NULL, '154093', '154093', 'Siti Amalia Nur Pratiwi', NULL, 'lab_ma', 1, '154093', '2qmmz', '57c94f576af51f6241c19abefdc9972b.jpg', NULL);
INSERT INTO `siswa` VALUES (2108, '327', NULL, '154097', '154097', 'Suci Indah Puspa Sari', NULL, 'lab_ma', 1, '154097', 'k6ump', '658553c8b2c791adba87f1c7eb1066fb.jpg', NULL);
INSERT INTO `siswa` VALUES (2109, '330', NULL, '154098', '154098', 'Syifa Fauziah ', NULL, 'lab_ma', 1, '154098', 'tl9yn', 'ac40784e57c14223b45b0130632a83d7.jpg', NULL);
INSERT INTO `siswa` VALUES (2110, '291', NULL, '154100', '154100', 'Tasya Nahwal Kamilah', NULL, 'lab_ma', 1, '154100', 'aqzyr', '5b6969af43fd82651ac6f7ceadfe9253.jpg', NULL);
INSERT INTO `siswa` VALUES (2112, '291', NULL, '154104', '154104', 'Widi Lestari', NULL, 'lab_ma', 1, '154104', 'd92qn', '873b24d8b30adac86ea9834503a6360b.jpg', NULL);
INSERT INTO `siswa` VALUES (2113, '332', NULL, '154004', '154004', 'Agisna Nurzaeni', NULL, 'lab_ma', 1, '154004', '98jcb', '0d683092eb951d92326b210eacfad96f.jpg', NULL);
INSERT INTO `siswa` VALUES (2114, '291', NULL, '154006', '154006', 'Alda Nur Alfi Lail', NULL, 'lab_ma', 1, '154006', 'nnz0g', '95637ad20f57aaba8cef8b276f1a38dc.jpg', NULL);
INSERT INTO `siswa` VALUES (2115, '330', NULL, '154008', '154008', 'Pindah - Alya Zalfa El Salsabila', NULL, 'lab_ma', 1, '154008', 'sftqi', '611586ad12b862230b9ab209a5e77a04.jpg', NULL);
INSERT INTO `siswa` VALUES (2116, '291', NULL, '154010', '154010', 'Anbar Safitri', NULL, 'lab_ma', 1, '154010', '9ce8q', 'e8cb3cb2e0458c673fc618bfc149c81c.jpg', NULL);
INSERT INTO `siswa` VALUES (2117, '327', NULL, '154017', '154017', 'Dedeh Winingsih', NULL, 'lab_ma', 1, '154017', '2rmc3', 'ef6b27f88d86c03bcf84641ba49c217e.JPG', NULL);
INSERT INTO `siswa` VALUES (2119, '330', NULL, '154021', '154021', 'Dina Fitri Akmalia', NULL, 'lab_ma', 1, '154021', 'y9kmn', '35e98dafd873d3dfb0481bf68eb58436.jpg', NULL);
INSERT INTO `siswa` VALUES (2120, '326', NULL, '154022', '154022', 'Dina Sofiah', NULL, 'lab_ma', 1, '154022', 'i86ja', '71702dc797142ce61826b7dcd68745a9.JPG', NULL);
INSERT INTO `siswa` VALUES (2121, '328', NULL, '154028', '154028', 'Fakhriani Maulidia Safitri', NULL, 'lab_ma', 1, '154028', 'f920j', '2ac7179e756f799ea01c2530dc3cff58.jpg', NULL);
INSERT INTO `siswa` VALUES (2122, '327', NULL, '154032', '154032', 'Fifi Rahayu', NULL, 'lab_ma', 1, '154032', 'yriw0', '6261e1513b906b73b4320ecd6cd9527e.JPG', NULL);
INSERT INTO `siswa` VALUES (2124, '330', NULL, '154038', '154038', 'Heni Nuraini Nasution', NULL, 'lab_ma', 1, '154038', 'tj4hb', 'ff9a7ac47cafd13141f9924c93c904fe.jpg', NULL);
INSERT INTO `siswa` VALUES (2126, '332', NULL, '154049', '154049', 'Laila Nur Fitriani', NULL, 'lab_ma', 1, '154049', 'skgl8', '420dc8e333f289da5a8aabc7054de27b.jpg', NULL);
INSERT INTO `siswa` VALUES (2128, '332', NULL, '154071', '154071', 'Nadiya Adawiyah Afifah', NULL, 'lab_ma', 1, '154071', '6gbrs', '87e9971d8396f4ed265c2ea7a6d91ca9.jpg', NULL);
INSERT INTO `siswa` VALUES (2129, '330', NULL, '154073', '154073', 'Pindah - Nina Resha', NULL, 'lab_ma', 1, '154073', 'wd24c', 'f93ad870e6a2117c25b01c2fb8f67273.jpg', NULL);
INSERT INTO `siswa` VALUES (2131, '330', NULL, '154078', '154078', 'Pipin Adelina', NULL, 'lab_ma', 1, '154078', 't0mcl', 'ac5b7cf4c88ec9da69ff58daf5873e92.jpg', NULL);
INSERT INTO `siswa` VALUES (2132, '330', NULL, '154082', '154082', 'Rabiah Al-adawiah', NULL, 'lab_ma', 1, '154082', '4aicc', 'e64fddb11c78becf07241c08c8411403.jpg', NULL);
INSERT INTO `siswa` VALUES (2133, '291', NULL, '154106', '154106', 'Rhahel Aulia Risya', NULL, 'lab_ma', 1, '154106', 'k2cntbh', '69c4ce4c0c1f8560cf55a1f9833f58d1.jpg', NULL);
INSERT INTO `siswa` VALUES (2134, '332', NULL, '154085', '154085', 'Rizky Amelia', NULL, 'lab_ma', 1, '154085', 'ldjo5', '0a1711d7eb4e07df04307e5cab450d01.jpg', NULL);
INSERT INTO `siswa` VALUES (2135, '332', NULL, '154088', '154088', 'Pindah - Sahla Safira', NULL, 'lab_ma', 1, '154088', 'edyn0', 'bccdb7e3d6c7536d570fa3b2f4ad001a.jpg', NULL);
INSERT INTO `siswa` VALUES (2136, '330', NULL, '154089', '154089', 'Selyka Widi Astuti', NULL, 'lab_ma', 1, '154089', 'jghuk', '1c84d6c55cccba39b11bc78484369d38.jpg', NULL);
INSERT INTO `siswa` VALUES (2137, '332', NULL, '154090', '154090', 'Septhia Azhary Layn', NULL, 'lab_ma', 1, '154090', 'kt7au', '8ea0b70ab720824fda82df522db6ad6f.jpg', NULL);
INSERT INTO `siswa` VALUES (2139, '330', NULL, '154099', '154099', 'Syifa Kamila', NULL, 'lab_ma', 1, '154099', 'gbys7', 'db61bcfb2ccf36a39e88838952919bd1.jpg', NULL);
INSERT INTO `siswa` VALUES (2140, '330', NULL, '154101', '154101', 'Vina Nurhaliza', NULL, 'lab_ma', 1, '154101', 'l0ch7', '3e34c6ffaca17ef1d3737395226a97b3.jpg', NULL);
INSERT INTO `siswa` VALUES (2141, '327', NULL, '154105', '154105', 'Yuliani Sentosa', NULL, 'lab_ma', 1, '154105', 'kgps1', '6b3bcfbcaac3c18d9454e44004b684ad.jpg', NULL);
INSERT INTO `siswa` VALUES (2868, '325', NULL, '136142', '136142', 'Muhammad Rifki Ilham', NULL, 'lab_ma', 1, '136142', 'r2in7', 'c2d963cfd47ee00be05b936e3ed0df49.jpg', NULL);
INSERT INTO `siswa` VALUES (2869, '328', NULL, '136011', '136011', 'Ahmad Fauzi', NULL, 'lab_ma', 1, '136011', '82j4v', 'dbb73b683b7348de670ba5163b6c1e30.JPG', NULL);
INSERT INTO `siswa` VALUES (2870, '325', NULL, '136116', '136116', 'M. Satya Dharma', NULL, 'lab_ma', 1, '136116', '41fmb', '60cb4dfcdfa8b9ecd29989ab098f2ff3.jpg', NULL);
INSERT INTO `siswa` VALUES (2871, '325', NULL, '136010', '136010', 'Ahlam Surya Husandi', NULL, 'lab_ma', 1, '136010', 'xqzsy', '6cb771d478d9cebd466ee8db31a9842c.JPG', NULL);
INSERT INTO `siswa` VALUES (2872, '328', NULL, '136106', '136106', 'M. Falih Temongmere', NULL, 'lab_ma', 1, '136106', '2jz4b', '3cff1035ade4a0435588a15e0a5ffd1e.jpg', NULL);
INSERT INTO `siswa` VALUES (2873, '328', NULL, '136026', '136026', 'ARYA SEPTO NUGRAHA', NULL, 'lab_ma', 1, '136026', 'i09u2', '9b2334ace8afb72a4d659579ab562f5c.jpg', NULL);
INSERT INTO `siswa` VALUES (2874, '325', NULL, '136115', '136115', 'M. Ryan Miftahul Ullum', NULL, 'lab_ma', 1, '136115', 'k6ais', '66eaee1400857fbcc9f59841b35b9e7b.jpg', NULL);
INSERT INTO `siswa` VALUES (2875, '325', NULL, '136089', '136089', 'JUAN MARINE HARIONO', NULL, 'lab_ma', 1, '136089', '81xbc', '91951674fc9936ad76b29a21ffeb75b0.JPG', NULL);
INSERT INTO `siswa` VALUES (2876, '325', NULL, '136097', '136097', 'Lutfi Sili Mauladi', NULL, 'lab_ma', 1, '136097', 'wacwb', 'ca936588c89cf67bd9968b4e59f9028a.JPG', NULL);
INSERT INTO `siswa` VALUES (2877, '325', NULL, '136169', '136169', 'Reza Mohamad Maulana', NULL, 'lab_ma', 1, '136169', '2repx', 'ac19d207903b7bc2c6d13ad0c60c5ad9.jpg', NULL);
INSERT INTO `siswa` VALUES (2878, '328', NULL, '136123', '136123', 'Mille Antonio', NULL, 'lab_ma', 1, '136123', 'u5xlq', '36c5d74b26c730d87b882df007b17041.jpg', NULL);
INSERT INTO `siswa` VALUES (2879, '325', NULL, '136181', '136181', 'Rizky Ichsan Tanring', NULL, 'lab_ma', 1, '136181', 'gnmax', 'ef4a0a92654ac7cb9efab85417450950.jpg', NULL);
INSERT INTO `siswa` VALUES (2880, '328', NULL, '136199', '136199', 'Syuhud Jidna Kusuma', NULL, 'lab_ma', 1, '136199', 'koydm', 'c7c072c3db274d516cf78424df3656df.jpg', NULL);
INSERT INTO `siswa` VALUES (2882, '328', NULL, '136212', '136212', 'Yusuf Habibie', NULL, 'lab_ma', 1, '136212', 'ytbg6', '641ef06c88e8d7ae3716f5e59ee67bfc.jpg', NULL);
INSERT INTO `siswa` VALUES (2884, '325', NULL, '136205', '136205', 'Wifda Nasrullah', NULL, 'lab_ma', 1, '136205', 'twg1f', 'bebd09635e7441e0072dc6479948f07b.jpg', NULL);
INSERT INTO `siswa` VALUES (2885, '328', NULL, '136197', '136197', 'Sultanin Jaya', NULL, 'lab_ma', 1, '136197', '2vfjg', 'e3f6a764fa73824ff9b25a17f3b277cb.jpg', NULL);
INSERT INTO `siswa` VALUES (2886, '328', NULL, '136188', '136188', 'Shafly Abdurrafi', NULL, 'lab_ma', 1, '136188', 'd2gno', 'f24c824edc0f84bba8150bd9f5ece1e9.jpg', NULL);
INSERT INTO `siswa` VALUES (2887, '325', NULL, '136180', '136180', 'Rizky Firstiansyah', NULL, 'lab_ma', 1, '136180', '9futr', '772925fa1b58d65affb578331a004608.jpg', NULL);
INSERT INTO `siswa` VALUES (2888, '325', NULL, '136124', '136124', 'Moch. Bayu Ramadhan', NULL, 'lab_ma', 1, '136124', 'mvafz', '13b4c890f993c93d59d26fbbb11d895c.JPG', NULL);
INSERT INTO `siswa` VALUES (2889, '325', NULL, '136126', '136126', 'Mochammad Rayhan Hardnur', NULL, 'lab_ma', 1, '136126', '75gwh', '8f0161941df3726c205b4abbd5fd9f24.JPG', NULL);
INSERT INTO `siswa` VALUES (2890, '325', NULL, '136178', '136178', 'Rizki Bagus Aminarto', NULL, 'lab_ma', 1, '136178', 'g14zs', '885b01f78e2881d2364d6f9189842c44.jpg', NULL);
INSERT INTO `siswa` VALUES (2891, '325', NULL, '136105', '136105', 'M. Fahri Rafifsyah', NULL, 'lab_ma', 1, '136105', 'wb6i5', '0437cb38f8cf8176b6aecf94b9bf99e3.jpg', NULL);
INSERT INTO `siswa` VALUES (2892, '325', NULL, '136051', '136051', 'Ergia Maulian', NULL, 'lab_ma', 1, '136051', 'ykao7', 'ae7870cd2f06c404a6004968afb3fb5d.JPG', NULL);
INSERT INTO `siswa` VALUES (2893, '325', NULL, '136129', '136129', 'Muchammad Syafiq Fathurrahman', NULL, 'lab_ma', 1, '136129', 'dqek1', 'cd220419344bd9d1e0899cea6736d671.jpg', NULL);
INSERT INTO `siswa` VALUES (2894, '328', NULL, '136039', '136039', 'Daffa Bagus Aji Pratama', NULL, 'lab_ma', 1, '136039', '4as7i', '27df3e5c9146c2e37ef760000d543e1d.JPG', NULL);
INSERT INTO `siswa` VALUES (2895, '325', NULL, '136131', '136131', 'Muhamad Iqbal Jamil', NULL, 'lab_ma', 1, '136131', 'yowd5', 'ada07f2b89f0f44978a17c703d12ed60.jpg', NULL);
INSERT INTO `siswa` VALUES (2896, '325', NULL, '136171', '136171', 'Ridho Valentino Supeno', NULL, 'lab_ma', 1, '136171', 't3p8d', 'acf3ac4f0136e9cf8b2ea84d6f78e2d3.jpg', NULL);
INSERT INTO `siswa` VALUES (2897, '328', NULL, '136112', '136112', 'M. Reyhan Maulana', NULL, 'lab_ma', 1, '136112', 'q8ako', 'b66a7b0ece5543d3baf8b8c2823df7d2.jpg', NULL);
INSERT INTO `siswa` VALUES (2898, '325', NULL, '136058', '136058', 'Faizhal Ilham Aditya', NULL, 'lab_ma', 1, '136058', '9hlus', 'bd2211ee285c4173c0a2956b35f31edc.JPG', NULL);
INSERT INTO `siswa` VALUES (2899, '328', NULL, '136168', '136168', 'Reza Adit Prasetyo', NULL, 'lab_ma', 1, '136168', 'jckvt', 'a4fdda9edecc4c45f1b88edf732c1f30.jpg', NULL);
INSERT INTO `siswa` VALUES (2900, '325', NULL, '136141', '136141', 'Muhammad Reno', NULL, 'lab_ma', 1, '136141', '22aet', '03c46e1fd2b3f8586bc73f78a193053e.jpg', NULL);
INSERT INTO `siswa` VALUES (2901, '325', NULL, '136140', '136140', 'Muhammad Raihan Azhar', NULL, 'lab_ma', 1, '136140', 'mbax8', '678fa888903f54e0f5da015add52e930.jpg', NULL);
INSERT INTO `siswa` VALUES (2902, '325', NULL, '136137', '136137', 'Muhammad Iqbal Fitriansyah', NULL, 'lab_ma', 1, '136137', 'hbt4t', 'c6c45543473de8986cbeb7d320e546fa.jpg', NULL);
INSERT INTO `siswa` VALUES (2903, '329', NULL, '136099', '136099', 'M Syadam Fahrian', NULL, 'lab_ma', 1, '136099', 'ci4d0', 'f366dc5511b7afdb205e27bbbd439e29.jpg', NULL);
INSERT INTO `siswa` VALUES (2904, '325', NULL, '136045', '136045', 'Dimas Aldi Rianto', NULL, 'lab_ma', 1, '136045', '5dmnd', '9f5d825964bfb14c232b6bf2d44c30de.JPG', NULL);
INSERT INTO `siswa` VALUES (2905, '325', NULL, '136075', '136075', 'Harun Robain', NULL, 'lab_ma', 1, '136075', '1e3a4', '2f4086c3f7dc6430896eb71ae2e59c79.JPG', NULL);
INSERT INTO `siswa` VALUES (2906, '325', NULL, '136081', '136081', 'Ilham Hafidhi', NULL, 'lab_ma', 1, '136081', 'ba8lm', '1b6aa2915c6da982b9d69f50ce6ff780.JPG', NULL);
INSERT INTO `siswa` VALUES (2907, '325', NULL, '136054', '136054', 'FADHIL MUHAMMAD', NULL, 'lab_ma', 1, '136054', 'f65xf', 'a47ae7b893c20ee408ba57a3851a18e6.JPG', NULL);
INSERT INTO `siswa` VALUES (2908, '328', NULL, '136134', '136134', 'Muhammad Fikri Istaqomali', NULL, 'lab_ma', 1, '136134', 'o3z82', '7d3f1155ce6b7d3c9538d05f369dcdf8.jpg', NULL);
INSERT INTO `siswa` VALUES (2909, '326', NULL, '136016', '136016', 'Alifia Raihani Azwar', NULL, 'lab_ma', 1, '136016', 'nctuf', 'dfdf13a850cce5323ecd1cd6721f1490.jpg', NULL);
INSERT INTO `siswa` VALUES (2910, '326', NULL, '136019', '136019', 'Aminah Tuzzahro', NULL, 'lab_ma', 1, '136019', '6affi', '62ad5a0f02b822ac3fd92f4028ddd6a9.jpg', NULL);
INSERT INTO `siswa` VALUES (2911, '327', NULL, '136022', '136022', 'Anisa Sri Rahayu', NULL, 'lab_ma', 1, '136022', '6370p', '19bfaf8fcbc0a0b9db2fcfafd330b69b.jpg', NULL);
INSERT INTO `siswa` VALUES (2912, '327', NULL, '136028', '136028', 'Assyifa Nur Fadhilah', NULL, 'lab_ma', 1, '136028', '9yhwc', '83153f66c7bea11e1d243152340d5ff8.jpg', NULL);
INSERT INTO `siswa` VALUES (2913, '326', NULL, '136032', '136032', 'Azka Sayyida Nafisa', NULL, 'lab_ma', 1, '136032', '7ccsl', 'f3fd757d89a88702c810343b23dea502.jpg', NULL);
INSERT INTO `siswa` VALUES (2914, '328', NULL, '136036', '136036', 'Bazliah Rusdi', NULL, 'lab_ma', 1, '136036', 'g9ehs', '92c4e32af56a0d81bd6c469e08002445.jpg', NULL);
INSERT INTO `siswa` VALUES (2915, '326', NULL, '136185', '136185', 'Salma Nur Aulia Maulida', NULL, 'lab_ma', 1, '136185', '99zvv', '3609fab562091bb723a16dd4c0a89892.jpg', NULL);
INSERT INTO `siswa` VALUES (2916, '327', NULL, '136191', '136191', 'Shoffana Muthiah Fadhilah', NULL, 'lab_ma', 1, '136191', 'h14wl', 'd47d68b770ee9634e7a97ae8b8aea6a6.jpg', NULL);
INSERT INTO `siswa` VALUES (2917, '326', NULL, '136193', '136193', 'Shohifah Quraniyah', NULL, 'lab_ma', 1, '136193', 'v95ot', '91a4fe70db3169a71126887f282375ca.jpg', NULL);
INSERT INTO `siswa` VALUES (2918, '327', NULL, '136200', '136200', 'Tjut Nyak Shahara', NULL, 'lab_ma', 1, '136200', 'ij5fj', '756e3bd922732b6086628d0dc4943b08.jpg', NULL);
INSERT INTO `siswa` VALUES (2919, '326', NULL, '136202', '136202', 'Tsaltsa Putri Ramadhany', NULL, 'lab_ma', 1, '136202', 'nmgup', '6c745551164b917c39152a0aaef7612d.jpg', NULL);
INSERT INTO `siswa` VALUES (2920, '327', NULL, '136049', '136049', 'DWI NUR ANGGITA', NULL, 'lab_ma', 1, '136049', 'wyztz', 'f75c62468cb6462adf0d020c4cf58a8c.jpg', NULL);
INSERT INTO `siswa` VALUES (2921, '328', NULL, '136001', '136001', 'Pindah - Aay Hariyanti Zuhroini', NULL, 'lab_ma', 1, '136001', 'cs88g', '0e47a7bf6b2be23a5294359359ff73d2.jpg', NULL);
INSERT INTO `siswa` VALUES (2922, '326', NULL, '136008', '136008', 'Aenun Rahmawati', NULL, 'lab_ma', 1, '136008', '8s37t', '7104a5674b3b11f10930616ad92eda2e.jpg', NULL);
INSERT INTO `siswa` VALUES (2923, '327', NULL, '136014', '136014', 'Alfiyaturrahmah', NULL, 'lab_ma', 1, '136014', 'tb4aw', '45a8622a20417e7cddef1cc17a0fd64f.jpg', NULL);
INSERT INTO `siswa` VALUES (2924, '328', NULL, '136182', '136182', 'Rizqi Alawiyah Wijaya', NULL, 'lab_ma', 1, '136182', 'w7npj', '6e8a61aa88f1f707a05d24dd94e8356f.jpg', NULL);
INSERT INTO `siswa` VALUES (2925, '326', NULL, '136020', '136020', 'Anelza Vierlianne', NULL, 'lab_ma', 1, '136020', 'vvucb', '0c8935fa6042a62260bdacafc2dae2d4.jpg', NULL);
INSERT INTO `siswa` VALUES (2926, '327', NULL, '136005', '136005', 'Adinda Fadhilah Azzahra', NULL, 'lab_ma', 1, '136005', '2dre8', '2cd7571b00794a5827c6e6959341e3bd.jpg', NULL);
INSERT INTO `siswa` VALUES (2927, '327', NULL, '136027', '136027', 'Asma Qonitah', NULL, 'lab_ma', 1, '136027', '3obsd', '7875d989987a9df513f4e6f78a7f84db.jpg', NULL);
INSERT INTO `siswa` VALUES (2928, '328', NULL, '136030', '136030', 'Ayu Kartika', NULL, 'lab_ma', 1, '136030', 'e0v5g', '04e0d174c3db937381789bccc0d25c95.jpg', NULL);
INSERT INTO `siswa` VALUES (2929, '328', NULL, '136062', '136062', 'Farihatul Kamila', NULL, 'lab_ma', 1, '136062', '8uegb', '6dca465f274f8c6571102d5b227354d3.jpg', NULL);
INSERT INTO `siswa` VALUES (2930, '328', NULL, '136044', '136044', 'Deva Nurkhalifah', NULL, 'lab_ma', 1, '136044', '100pt', '7656191ebc5018fe1f3a079067dc4e47.jpg', NULL);
INSERT INTO `siswa` VALUES (2931, '326', NULL, '136047', '136047', 'Dina Septiani', NULL, 'lab_ma', 1, '136047', 'o7ifa', '153e58896430b7a96de958b34709ed03.jpg', NULL);
INSERT INTO `siswa` VALUES (2932, '326', NULL, '136079', '136079', 'Humairo Azizah', NULL, 'lab_ma', 1, '136079', 'c22tz', '9a5003152763595732521caedf3cd4dd.jpg', NULL);
INSERT INTO `siswa` VALUES (2933, '326', NULL, '136057', '136057', 'Fahriza Hazrina Zain', NULL, 'lab_ma', 1, '136057', 'pp1dn', 'a759f98d5e18dece5a214db95c24bab0.jpg', NULL);
INSERT INTO `siswa` VALUES (2934, '327', NULL, '136084', '136084', 'Intan Fairuziah', NULL, 'lab_ma', 1, '136084', 'iw52f', '5d1d9e951c1cb3a16ad516fb20d6f68e.jpg', NULL);
INSERT INTO `siswa` VALUES (2935, '327', NULL, '136093', '136093', 'Laluna Salsa Midika', NULL, 'lab_ma', 1, '136093', 'uemh5', '8663d5217fcb7a044aab22e0668d3284.jpg', NULL);
INSERT INTO `siswa` VALUES (2936, '326', NULL, '136096', '136096', 'Lisania Amanah', NULL, 'lab_ma', 1, '136096', 'yjbh1', '66756d75dae250e0f6d4be8ddb66ffec.jpg', NULL);
INSERT INTO `siswa` VALUES (2937, '326', NULL, '136146', '136146', 'Mutiara Nur Azizah', NULL, 'lab_ma', 1, '136146', '6wvv8', 'b463516c08da930912e9c0458d587e2b.jpg', NULL);
INSERT INTO `siswa` VALUES (2938, '327', NULL, '136152', '136152', 'Nila Fauzi Nur Saadah', NULL, 'lab_ma', 1, '136152', '7jerh', 'cd35b8810bb5f3b1fe3006f10d71cc12.jpg', NULL);
INSERT INTO `siswa` VALUES (2939, '327', NULL, '136161', '136161', 'Puput Mutia Anisa', NULL, 'lab_ma', 1, '136161', 'cuqb8', 'a93ec30f9085dc7baebb5c55db1ae1d2.jpg', NULL);
INSERT INTO `siswa` VALUES (2940, '326', NULL, '136164', '136164', 'Raden Salsabila Nurul Amin', NULL, 'lab_ma', 1, '136164', 'gbmjt', '88f1d79e3a0afde88bd17626f401d280.jpg', NULL);
INSERT INTO `siswa` VALUES (2942, '327', NULL, '136067', '136067', 'Fismy Faturrahmy F.', NULL, 'lab_ma', 1, '136067', '9etjf', '9075da2398eeab7ed001e3e7ff115eb5.jpg', NULL);
INSERT INTO `siswa` VALUES (2943, '327', NULL, '136120', '136120', 'Maftuhatur Rusydah', NULL, 'lab_ma', 1, '136120', '31m8n', 'bbef84ad00e8042d0232d4633e9d0df7.jpg', NULL);
INSERT INTO `siswa` VALUES (2944, '326', NULL, '136156', '136156', 'Nur Alya Muyasar', NULL, 'lab_ma', 1, '136156', 'mb5dh', '015595c52ba80c68d387d836c202a433.jpg', NULL);
INSERT INTO `siswa` VALUES (2945, '327', NULL, '136157', '136157', 'Nur Azizah Hafidzah', NULL, 'lab_ma', 1, '136157', '2tikm', '2425b8b0e813a2d1d36fbdc57159296c.jpg', NULL);
INSERT INTO `siswa` VALUES (2946, '326', NULL, '136160', '136160', 'Nurul Nadhilah', NULL, 'lab_ma', 1, '136160', '9s1g7', '4ab198fc916ced54f98b7f5ced128e63.jpg', NULL);
INSERT INTO `siswa` VALUES (2948, '326', NULL, '136052', '136052', 'ERSA SANIA NASUTION', NULL, 'lab_ma', 1, '136052', 'vtr2r', 'b8500aa6f1ae203bfa81922ef9df7502.jpg', NULL);
INSERT INTO `siswa` VALUES (2949, '327', NULL, '136055', '136055', 'FADHILATUZ ZAHWA          ', NULL, 'lab_ma', 1, '136055', '0hekf', 'efca73c789f75495ec32214543b746b1.jpg', NULL);
INSERT INTO `siswa` VALUES (2950, '327', NULL, '136149', '136149', 'NAJMIA LATIFARANI', NULL, 'lab_ma', 1, '136149', 'fpsrd', '4f2448a26ee7e179718b6f64cea5afaf.jpg', NULL);
INSERT INTO `siswa` VALUES (2951, '327', NULL, '136153', '136153', 'NILAM FASSA MAHARANI', NULL, 'lab_ma', 1, '136153', '8y41v', '8aaa826008fcd4e4a741fe83f08f7545.jpg', NULL);
INSERT INTO `siswa` VALUES (2952, '328', NULL, '136162', '136162', 'PUTRI NUR WAHIDAH', NULL, 'lab_ma', 1, '136162', 'bcbf9', '6874e67f34572cbd2ea9ccad0d63e0cf.jpg', NULL);
INSERT INTO `siswa` VALUES (2953, '332', NULL, '136087', '136087', 'ISMAYA WARDATI BASIMAH', NULL, 'lab_ma', 1, '136087', 'l2ncm', '97857433e661bdc3e3459c41667bc9ac.jpg', NULL);
INSERT INTO `siswa` VALUES (2954, '327', NULL, '136190', '136190', 'SHEYMA MAHARANI PUTRI', NULL, 'lab_ma', 1, '136190', 'nmwg9', '2ad312ac025ed3c6fe8ac56fefae716a.jpg', NULL);
INSERT INTO `siswa` VALUES (2955, '326', NULL, '136122', '136122', 'MEGA CAHYA TSANIA', NULL, 'lab_ma', 1, '136122', 'y1tc1', '8a1d29f716da0fef18b4aa5dbd547447.jpg', NULL);
INSERT INTO `siswa` VALUES (2956, '327', NULL, '136145', '136145', 'MUTIARA DWI SANDHI', NULL, 'lab_ma', 1, '136145', 'eivg7', 'd0bd41de285d5e1e8f4fd63022a8e873.jpg', NULL);
INSERT INTO `siswa` VALUES (2957, '326', NULL, '136147', '136147', 'NADZIRA AMALIA RIZKIE', NULL, 'lab_ma', 1, '136147', '4czug', '26a46f2051b49f074bd031aac2297114.jpg', NULL);
INSERT INTO `siswa` VALUES (2958, '328', NULL, '136155', '136155', 'NUR ALFIAH ZULMI', NULL, 'lab_ma', 1, '136155', '4vkim', '3d0db95eee8a105200ff4c8f1c114bfd.jpg', NULL);
INSERT INTO `siswa` VALUES (2959, '327', NULL, '136066', '136066', 'Firda Sabrina Amir', NULL, 'lab_ma', 1, '136066', '0rzcn', 'ebf3a8ca041b429605dd25c95008f8b3.jpg', NULL);
INSERT INTO `siswa` VALUES (2960, '326', NULL, '136042', '136042', 'Davina Aurelia Talaar          ', NULL, 'lab_ma', 1, '136042', '9ahbq', '50b47a7fe6bbe35c8c74748fea2bf68a.jpg', NULL);
INSERT INTO `siswa` VALUES (2961, '328', NULL, '136214', '136214', 'Zulfah Nabilah', NULL, 'lab_ma', 1, '136214', 'w2vdb', '68d8b5f0bf2ddae39a40222e030f04ed.jpg', NULL);
INSERT INTO `siswa` VALUES (2962, '327', NULL, '136204', '136204', 'Widya Ayu Fadilla', NULL, 'lab_ma', 1, '136204', 'nlh8u', '305c4f3cfd57a3a37973ca3cc3cd3e4b.jpg', NULL);
INSERT INTO `siswa` VALUES (2963, '326', NULL, '136201', '136201', 'Trisnawati Dewi', NULL, 'lab_ma', 1, '136201', 'nsfzf', 'd6f06cf3de130ee62272afc0a3d53046.jpg', NULL);
INSERT INTO `siswa` VALUES (2964, '327', NULL, '136198', '136198', 'Syifa Nurul Aini syafitri', NULL, 'lab_ma', 1, '136198', 'jaq0u', '808f97635cf04ece7a8b2e7eb28abe7e.jpg', NULL);
INSERT INTO `siswa` VALUES (2965, '326', NULL, '136196', '136196', 'Sonia Agustina', NULL, 'lab_ma', 1, '136196', 'owrzf', '4aeef78cbaf9800bcad8f9ef034c443c.jpg', NULL);
INSERT INTO `siswa` VALUES (2966, '327', NULL, '136195', '136195', 'Siti Nurrifdah Pertiwi', NULL, 'lab_ma', 1, '136195', 'gc9v0', '5920bf02026320acbd10ad9c23d0d07b.jpg', NULL);
INSERT INTO `siswa` VALUES (2967, '326', NULL, '136194', '136194', 'Siti Nurfadilah', NULL, 'lab_ma', 1, '136194', 'zsay2', '4d8db6552c3656a07c639b90c3e95f18.jpg', NULL);
INSERT INTO `siswa` VALUES (2968, '328', NULL, '136189', '136189', 'Shahifah Faiza Ahfaliyah', NULL, 'lab_ma', 1, '136189', 'ebnct', '78495ff27b310986fc72fed368fbe11c.jpg', NULL);
INSERT INTO `siswa` VALUES (2969, '326', NULL, '136186', '136186', 'Salsabila Azkya Muslim', NULL, 'lab_ma', 1, '136186', 'mkim1', 'd87323f0d48f6c3561811223d4227a99.jpg', NULL);
INSERT INTO `siswa` VALUES (2970, '327', NULL, '136183', '136183', 'Rosalina Dwi Agustin', NULL, 'lab_ma', 1, '136183', 'l4hv6', '88a3f1cfa38348b474a5bdb799397d33.jpg', NULL);
INSERT INTO `siswa` VALUES (2971, '326', NULL, '136170', '136170', 'Rida Saniya Raudoh', NULL, 'lab_ma', 1, '136170', 'xqngh', '3875c206f52c5fbeafda7772710d29cc.jpg', NULL);
INSERT INTO `siswa` VALUES (2972, '327', NULL, '136167', '136167', 'Ratna', NULL, 'lab_ma', 1, '136167', 'sdqfp', '9f1d0b31077b3947b52aaa0bafcf632b.jpg', NULL);
INSERT INTO `siswa` VALUES (2973, '326', NULL, '136159', '136159', 'Nurlela', NULL, 'lab_ma', 1, '136159', '8u620', '1a34c38f2f05cc5bcd23be0fa9ebb05b.jpg', NULL);
INSERT INTO `siswa` VALUES (2974, '326', NULL, '136021', '136021', 'Anggia Murni', NULL, 'lab_ma', 1, '136021', '3xu4j', '8ae96d306629cd836ccf20b5b1169376.jpg', NULL);
INSERT INTO `siswa` VALUES (2975, '327', NULL, '136006', '136006', 'Adinda Yasmina Ramadanti          ', NULL, 'lab_ma', 1, '136006', '9w3w0', '877af67c982acbd6c50487742e36e26f.jpg', NULL);
INSERT INTO `siswa` VALUES (2976, '330', NULL, '136018', '136018', 'Amelia Ramadhani          ', NULL, 'lab_ma', 1, '136018', 'asi1w', '6979b10d6f1982caf496b5f379536734.jpg', NULL);
INSERT INTO `siswa` VALUES (2977, '326', NULL, '136035', '136035', 'Azzahra Hapsya Aulia', NULL, 'lab_ma', 1, '136035', '0sr1e', 'f3c95701bc593dae02b51b1abe2b4328.jpg', NULL);
INSERT INTO `siswa` VALUES (2978, '327', NULL, '136050', '136050', 'Elsa Widiyanti          ', NULL, 'lab_ma', 1, '136050', '8ljez', '56eb4d50d4ca04353d5725a15943ccb3.jpg', NULL);
INSERT INTO `siswa` VALUES (2979, '326', NULL, '136151', '136151', 'Ni Dewi Inayah H.K', NULL, 'lab_ma', 1, '136151', '8an01', '52d480d9ad25e80804a0fe634eaaf1aa.jpg', NULL);
INSERT INTO `siswa` VALUES (2980, '327', NULL, '136056', '136056', 'Fahra Aisha Fajrin', NULL, 'lab_ma', 1, '136056', 'hhyy6', 'a224478549752c58b0b2598aca794d0f.jpg', NULL);
INSERT INTO `siswa` VALUES (2981, '326', NULL, '136059', '136059', 'Farah Fauziah', NULL, 'lab_ma', 1, '136059', 'yfqu7', '2997000c8ddd6dc2a16ff623369d68f3.jpg', NULL);
INSERT INTO `siswa` VALUES (2982, '327', NULL, '136060', '136060', 'Farahiyah Azzyati ', NULL, 'lab_ma', 1, '136060', 'shgoy', 'aa57c11c34c958be99e14cbdc693793a.jpg', NULL);
INSERT INTO `siswa` VALUES (2983, '326', NULL, '136063', '136063', 'Febiyanti Tiara Suci ', NULL, 'lab_ma', 1, '136063', 'yizda', '4b164b36f1972136950764b277696196.jpg', NULL);
INSERT INTO `siswa` VALUES (2984, '327', NULL, '136074', '136074', 'Hanifa Zahratunnisa', NULL, 'lab_ma', 1, '136074', 'mn82m', '0863085b3984813df59deb11b157717f.jpg', NULL);
INSERT INTO `siswa` VALUES (2985, '326', NULL, '136088', '136088', 'Jihan Reviandra', NULL, 'lab_ma', 1, '136088', 'e9l0a', '672318fe34a19680f026f0289818180d.jpg', NULL);
INSERT INTO `siswa` VALUES (2986, '328', NULL, '136094', '136094', 'Latifa Rahmah Laila', NULL, 'lab_ma', 1, '136094', '05ceo', '4b66af1c4333af614051924a450fe1d1.jpg', NULL);
INSERT INTO `siswa` VALUES (2987, '327', NULL, '136121', '136121', 'Mahliya Shafa', NULL, 'lab_ma', 1, '136121', 'e3nf0', '874a84f20f70ef1c4ed6a8f31c9322c2.jpg', NULL);
INSERT INTO `siswa` VALUES (2988, '331', NULL, '136125', '136125', 'Moch.Alwi Prysas', NULL, 'lab_ma', 1, '136125', 'fbfnw', 'd75c182c6626af3e60605d14a3b31b96.jpg', NULL);
INSERT INTO `siswa` VALUES (2989, '328', NULL, '136209', '136209', 'Wildan M Fahmi', NULL, 'lab_ma', 1, '136209', 'u5c8f', '73089f37f6e82d76cc427729daac95b0.jpg', NULL);
INSERT INTO `siswa` VALUES (2990, '331', NULL, '136013', '136013', 'Alfian Darmawan', NULL, 'lab_ma', 1, '136013', '2bbe3', '945e442484c8ef4c218746d3dc54ab1b.jpg', NULL);
INSERT INTO `siswa` VALUES (2992, '331', NULL, '136023', '136023', 'Ardhi Dwi Andika', NULL, 'lab_ma', 1, '136023', 'vmfp6', '44f8123e12b6a3e4941138d78f0ed305.jpg', NULL);
INSERT INTO `siswa` VALUES (2994, '331', NULL, '136154', '136154', 'Nizar Sadat', NULL, 'lab_ma', 1, '136154', 'q12oe', 'd63269802863456bc3338f82c1b8ae81.JPG', NULL);
INSERT INTO `siswa` VALUES (2995, '329', NULL, '136128', '136128', 'Muaz Aziz', NULL, 'lab_ma', 1, '136128', '17eoo', 'a21459db31a137183aa90dba319cae44.jpg', NULL);
INSERT INTO `siswa` VALUES (2996, '329', NULL, '136172', '136172', 'Ridmi Irfan', NULL, 'lab_ma', 1, '136172', '0adwo', '4071d141713c55b287975ac3786cb8d4.jpg', NULL);
INSERT INTO `siswa` VALUES (2997, '329', NULL, '136174', '136174', 'Rio Zulqarrnain Hidayat', NULL, 'lab_ma', 1, '136174', 'qdwum', 'f0afcc52438d48fc3fbf93a3ab36d0c0.jpg', NULL);
INSERT INTO `siswa` VALUES (2998, '329', NULL, '136110', '136110', 'M. Rama Saputra', NULL, 'lab_ma', 1, '136110', 'gqb7s', '676733702486a9f642a2cedc9ee06452.jpg', NULL);
INSERT INTO `siswa` VALUES (2999, '331', NULL, '136046', '136046', 'Din Ruchdini Julianur', NULL, 'lab_ma', 1, '136046', '02akz', '7e2dd9c6a034982b4ab9747795333a33.jpg', NULL);
INSERT INTO `siswa` VALUES (3000, '329', NULL, '136065', '136065', 'Fikri Akbar Fadilah', NULL, 'lab_ma', 1, '136065', '7zfzq', '5ba09eadc231a0a88f0f7294671f2132.jpg', NULL);
INSERT INTO `siswa` VALUES (3001, '331', NULL, '136206', '136206', 'Wildan Isman N', NULL, 'lab_ma', 1, '136206', '9obzl', 'd1aba00ee95766f3dff895135fd359b8.jpg', NULL);
INSERT INTO `siswa` VALUES (3002, '331', NULL, '136111', '136111', 'M. Reifa Nizi Al -Wafa', NULL, 'lab_ma', 1, '136111', 'o3asp', '0ec025051ba8b63499aad187b4ac8419.jpg', NULL);
INSERT INTO `siswa` VALUES (3003, '329', NULL, '136078', '136078', 'Hilmawan Tirta Nirwana', NULL, 'lab_ma', 1, '136078', 'u4com', '32fc0798e5ce05003710326bef673aa3.jpg', NULL);
INSERT INTO `siswa` VALUES (3004, '329', NULL, '136109', '136109', 'M. Parkhan Izam Mugnil Auli', NULL, 'lab_ma', 1, '136109', 'cqrcv', '22bcf8fb0453d56376976ab9efe9e081.jpg', NULL);
INSERT INTO `siswa` VALUES (3005, '331', NULL, '136113', '136113', 'M. Rizal Baesuni', NULL, 'lab_ma', 1, '136113', 'm14ev', 'db6f6de138b8595b0101e68ea7bf2e35.jpg', NULL);
INSERT INTO `siswa` VALUES (3006, '329', NULL, '136143', '136143', 'Muhammad Zidane Ar-Rizqi', NULL, 'lab_ma', 1, '136143', 'kpi3z', 'bdb8dd2df8c7bb093616d5fe9224aeb9.jpg', NULL);
INSERT INTO `siswa` VALUES (3008, '329', NULL, '136127', '136127', 'Mohammad Alfian Dhiya Ulhaq', NULL, 'lab_ma', 1, '136127', '7zdhj', 'f7854eef060997f910163a039fe23dff.jpg', NULL);
INSERT INTO `siswa` VALUES (3010, '331', NULL, '136130', '136130', 'Muhamad Bahrul Ulum', NULL, 'lab_ma', 1, '136130', 'gq59m', '490adfe24626f8935687fb8abf7b1d08.jpg', NULL);
INSERT INTO `siswa` VALUES (3012, '329', NULL, '136210', '136210', 'Yasser Al-Fawwaz', NULL, 'lab_ma', 1, '136210', '2skdm', '769063fb84ea63bf4e643241572c365e.jpg', NULL);
INSERT INTO `siswa` VALUES (3013, '328', NULL, '136207', '136207', 'Wildan Khoir Lubis', NULL, 'lab_ma', 1, '136207', '3ce5m', 'af281a5416221db5e9833ec7b7b14e2e.jpg', NULL);
INSERT INTO `siswa` VALUES (3014, '329', NULL, '136184', '136184', 'Saepul Rhohman', NULL, 'lab_ma', 1, '136184', '5ym5y', '5319a5e475b9461f546ccde7a99c1f04.jpg', NULL);
INSERT INTO `siswa` VALUES (3015, '331', NULL, '136179', '136179', 'Rizky Aditya Pratama', NULL, 'lab_ma', 1, '136179', 'idjv2', '9f53db6d3923aab5dc1656beeca3e66d.jpg', NULL);
INSERT INTO `siswa` VALUES (3016, '331', NULL, '136173', '136173', 'Ridwan Maulana', NULL, 'lab_ma', 1, '136173', 'z7kl8', '74a4bba550a94e322d4d0d7e0bd1db7a.jpg', NULL);
INSERT INTO `siswa` VALUES (3017, '329', NULL, '136166', '136166', 'Randi Saputra', NULL, 'lab_ma', 1, '136166', 'lfmrf', 'c2bb2f4cbc40fd7e984e68c5d4e0a3f7.jpg', NULL);
INSERT INTO `siswa` VALUES (3018, '331', NULL, '136117', '136117', 'M. Ziddan Ali', NULL, 'lab_ma', 1, '136117', '83hw5', 'f54d0ca36d23d53e4abfa9f20d5bed8f.jpg', NULL);
INSERT INTO `siswa` VALUES (3019, '331', NULL, '136118', '136118', 'M.Rijal Aufar ', NULL, 'lab_ma', 1, '136118', 'yxzxp', '9a008c2bf7fef92f98e501c8c1f905a2.jpg', NULL);
INSERT INTO `siswa` VALUES (3021, '329', NULL, '136004', '136004', 'Adam Ismail', NULL, 'lab_ma', 1, '136004', '6jmo6', 'a91e469f375a81e173135f281c4b75e7.jpg', NULL);
INSERT INTO `siswa` VALUES (3022, '329', NULL, '136025', '136025', 'Arief Firmansyah', NULL, 'lab_ma', 1, '136025', 'dfki7', '07bd3d6a4e833a3c188c54262c1deaa4.jpg', NULL);
INSERT INTO `siswa` VALUES (3023, '331', NULL, '136031', '136031', 'Azka Muzakkie', NULL, 'lab_ma', 1, '136031', 'm46sv', 'e7e4742d47e8d883ceb8e234af23c4b4.jpg', NULL);
INSERT INTO `siswa` VALUES (3024, '331', NULL, '136040', '136040', 'Dalfajar Ramdan Lesmana', NULL, 'lab_ma', 1, '136040', '8nn54', '89c8b78bb08a0ce3ce447a949e73dac5.jpg', NULL);
INSERT INTO `siswa` VALUES (3025, '329', NULL, '136041', '136041', 'Dandy Amiarno Putra', NULL, 'lab_ma', 1, '136041', 'abwxu', '0ea8c00ae6b5792a9e74229c0e6369c7.jpg', NULL);
INSERT INTO `siswa` VALUES (3026, '331', NULL, '136069', '136069', 'Guruh Mulyo Setyo Pratama', NULL, 'lab_ma', 1, '136069', 'penn2', 'c497be8358a6959d6f02fc314a1a4a3e.jpg', NULL);
INSERT INTO `siswa` VALUES (3027, '331', NULL, '136092', '136092', 'Krisnanta Rifky Noor F.', NULL, 'lab_ma', 1, '136092', 'mchjb', 'bf237792bbf085f52db1aad08a5f1cfd.jpg', NULL);
INSERT INTO `siswa` VALUES (3028, '329', NULL, '136107', '136107', 'M. Firmansyah', NULL, 'lab_ma', 1, '136107', 'nu6v0', '75c9a441b4cb9938f4cdbb03c15a9446.jpg', NULL);
INSERT INTO `siswa` VALUES (3029, '331', NULL, '136002', '136002', 'Abdaur Rizki Marwan', NULL, 'lab_ma', 1, '136002', 'ytdqh', 'f9de61f1250efaeaa70e6b28e0bbdbb8.JPG', NULL);
INSERT INTO `siswa` VALUES (3030, '331', NULL, '136086', '136086', 'Irsya Maulana', NULL, 'lab_ma', 1, '136086', 'rlsfx', '6f887e363538b6997fb9b9a7f5228d78.JPG', NULL);
INSERT INTO `siswa` VALUES (3031, '331', NULL, '136012', '136012', 'Ahmad Rosadi', NULL, 'lab_ma', 1, '136012', '2zakq', '1052fb3cad409304d064e53672216235.JPG', NULL);
INSERT INTO `siswa` VALUES (3033, '331', NULL, '136165', '136165', 'Raihan Alif Sabili', NULL, 'lab_ma', 1, '136165', '1teuq', '248f82b6a2c4ee8ee0e64e1d16dc85df.JPG', NULL);
INSERT INTO `siswa` VALUES (3034, '332', NULL, '136009', '136009', 'Agnia Mutmainah', NULL, 'lab_ma', 1, '136009', 'bz40b', '2a818993b544b3622d943e493779bbfa.jpg', NULL);
INSERT INTO `siswa` VALUES (3035, '331', NULL, '136139', '136139', 'Muhammad Raihan', NULL, 'lab_ma', 1, '136139', 'zdw1g', 'da6761d0c27038dff50b932013a4d937.JPG', NULL);
INSERT INTO `siswa` VALUES (3036, '331', NULL, '136053', '136053', 'Fachri Alfianza', NULL, 'lab_ma', 1, '136053', 'hktpt', '4e9499b52e3c760bb0d6dc9e93eed5d2.JPG', NULL);
INSERT INTO `siswa` VALUES (3038, '330', NULL, '136029', '136029', 'Auliya Azhar', NULL, 'lab_ma', 1, '136029', 'e4jbg', '4b0ae45d4fcdd9edd80eb1165147091f.jpg', NULL);
INSERT INTO `siswa` VALUES (3039, '332', NULL, '136033', '136033', 'Azka Widiyani', NULL, 'lab_ma', 1, '136033', 'mtaml', '5585b453a52bdfa9da3baccb9c923c34.jpg', NULL);
INSERT INTO `siswa` VALUES (3040, '330', NULL, '136061', '136061', 'Farhatun Nazila', NULL, 'lab_ma', 1, '136061', 'rn9tb', '721825919c42af62d3ece15623cb82f4.jpg', NULL);
INSERT INTO `siswa` VALUES (3041, '331', NULL, '136072', '136072', 'Hamzah Yaqub', NULL, 'lab_ma', 1, '136072', 'hbbmk', '0ee545ae8493aa516dcd22bc86d9497e.JPG', NULL);
INSERT INTO `siswa` VALUES (3042, '330', NULL, '136083', '136083', 'Indah Fadilah Rahmah', NULL, 'lab_ma', 1, '136083', '2etu7', '2980ea100a9be4a4a33cef46cb787fdf.jpg', NULL);
INSERT INTO `siswa` VALUES (3043, '329', NULL, '136073', '136073', 'Handika Maulana Salim', NULL, 'lab_ma', 1, '136073', 'jpqne', 'afc318f03f79ebe5f8f0057ff28a4f86.JPG', NULL);
INSERT INTO `siswa` VALUES (3044, '330', NULL, '136090', '136090', 'Khoirunnisa Eka Rozana', NULL, 'lab_ma', 1, '136090', 'yvpyl', '79fb16aaefa88567336013d6d949cacb.jpg', NULL);
INSERT INTO `siswa` VALUES (3045, '331', NULL, '136080', '136080', 'Ilham Bagus Fitriyanto', NULL, 'lab_ma', 1, '136080', 'dskop', 'ca086fcf4fcbfca8724f5e3e067b2934.JPG', NULL);
INSERT INTO `siswa` VALUES (3046, '332', NULL, '136150', '136150', 'Najwa El Shadra Faaza', NULL, 'lab_ma', 1, '136150', 'rgq8q', '15fc823d8519e3b869f1af7c3b36d073.jpg', NULL);
INSERT INTO `siswa` VALUES (3047, '332', NULL, '136176', '136176', 'Rizka Alvi Maulida', NULL, 'lab_ma', 1, '136176', 'ym0jl', '2539fef31db99b3dc5c62baea3ca7b80.jpg', NULL);
INSERT INTO `siswa` VALUES (3048, '330', NULL, '136187', '136187', 'Sarah Latifah', NULL, 'lab_ma', 1, '136187', 'jbnyg', 'df16ea06bd07904f23d500adf50175b4.jpg', NULL);
INSERT INTO `siswa` VALUES (3049, '329', NULL, '136007', '136007', 'Adithia Ramdhani Ariansyah', NULL, 'lab_ma', 1, '136007', 'exodp', 'cc37d28d894ef927fc01a8c96902c47d.JPG', NULL);
INSERT INTO `siswa` VALUES (3050, '329', NULL, '136015', '136015', 'Alif Naufal Teguh Putra', NULL, 'lab_ma', 1, '136015', '9w97i', '5565643c6b0fcf9a02611a839cc05832.JPG', NULL);
INSERT INTO `siswa` VALUES (3051, '331', NULL, '136034', '136034', 'Azmi Shibba Izzuddin', NULL, 'lab_ma', 1, '136034', '9vj6j', '929f024bdc7d5705863d79eec0a6ebcf.JPG', NULL);
INSERT INTO `siswa` VALUES (3052, '329', NULL, '136043', '136043', 'DENDY RAMADHAN', NULL, 'lab_ma', 1, '136043', 'pv0az', '9ad87f1af89c8d928ae0b2b36dbb43f5.JPG', NULL);
INSERT INTO `siswa` VALUES (3053, '329', NULL, '136064', '136064', 'Feri Erlangga', NULL, 'lab_ma', 1, '136064', 'og2me', 'ea8bd2e4ec2d28364e327f4079667c31.JPG', NULL);
INSERT INTO `siswa` VALUES (3055, '331', NULL, '136076', '136076', 'Hendri Mohamad Aprila', NULL, 'lab_ma', 1, '136076', 'vfy2m', '4384c42cb2d0426256a309030bb01d8b.JPG', NULL);
INSERT INTO `siswa` VALUES (3056, '329', NULL, '136077', '136077', 'Hilman Maulana (r)', NULL, 'lab_ma', 1, '136077', 'a36rn', '7ebc4d1c9f407268c5615e1c72b0e84e.JPG', NULL);
INSERT INTO `siswa` VALUES (3057, '329', NULL, '136098', '136098', 'Luthfi Abdurrahman', NULL, 'lab_ma', 1, '136098', 'b81jv', '', NULL);
INSERT INTO `siswa` VALUES (3058, '329', NULL, '136082', '136082', 'Ilham Wahdi Kusuma', NULL, 'lab_ma', 1, '136082', '8kssi', 'a91c859e39de61fece83892313846f8a.JPG', NULL);
INSERT INTO `siswa` VALUES (3059, '332', NULL, '136163', '136163', 'Pindah - Qoniatul Fajriah', NULL, 'lab_ma', 1, '136163', 'hajlz', '9bc659842382842da089577cef5e5309.jpg', NULL);
INSERT INTO `siswa` VALUES (3060, '330', NULL, '136048', '136048', 'Dinda Aulia Suntari', NULL, 'lab_ma', 1, '136048', '7j1z6', '93adbe728acf8ea4531e78fdfbda34aa.jpg', NULL);
INSERT INTO `siswa` VALUES (3062, '329', NULL, '136104', '136104', 'M. FADLY', NULL, 'lab_ma', 1, '136104', 'pfq95', 'd9f3fa9798729bb26e63d72752772c5a.JPG', NULL);
INSERT INTO `siswa` VALUES (3063, '330', NULL, '136213', '136213', 'Zahra Nurchaliza', NULL, 'lab_ma', 1, '136213', '7o6o8', '04cf097362266e5b96b4ff7a1a7a11e9.jpg', NULL);
INSERT INTO `siswa` VALUES (3064, '332', NULL, '136203', '136203', 'Wanda Kholidah', NULL, 'lab_ma', 1, '136203', 'd1fxg', '8e5526f8a120fe91bcf0eb5e52b302ba.jpg', NULL);
INSERT INTO `siswa` VALUES (3065, '332', NULL, '136144', '136144', 'MULYANI', NULL, 'lab_ma', 1, '136144', 't1na2', '2c4678ce2c4f53cafac533d788485907.jpg', NULL);
INSERT INTO `siswa` VALUES (3066, '331', NULL, '136133', '136133', 'MUHAMMAD FARHAN YAZID', NULL, 'lab_ma', 1, '136133', 'h5p5d', 'eeeb7ac44984ff208e5131201a773155.JPG', NULL);
INSERT INTO `siswa` VALUES (3067, '330', NULL, '136119', '136119', 'Madihah Kanta', NULL, 'lab_ma', 1, '136119', 'v3ea2', '68e98f6c75565a0ecc75d0be6052f46b.jpg', NULL);
INSERT INTO `siswa` VALUES (3068, '332', NULL, '136095', '136095', 'Lilih Sumirat', NULL, 'lab_ma', 1, '136095', '90qw9', '5d71428a16d74357476c20dc672cfb00.jpg', NULL);
INSERT INTO `siswa` VALUES (3069, '329', NULL, '136135', '136135', 'Muhammad Haekal Al-Farizi', NULL, 'lab_ma', 1, '136135', '33mab', '56c4f0b53286e47dd978bc7077d8952d.JPG', NULL);
INSERT INTO `siswa` VALUES (3071, '332', NULL, '136068', '136068', 'Grace Sania Effendy', NULL, 'lab_ma', 1, '136068', 'a1w0e', 'afa32cbccab045861adf953d5732e207.jpg', NULL);
INSERT INTO `siswa` VALUES (3072, '332', NULL, '154080', '154080', 'Putri Puspitasari', NULL, 'lab_ma', 1, '154080', 'qh6bj', 'a0ce036a81f09bb963402cac8349eecf.jpg', NULL);
INSERT INTO `siswa` VALUES (3088, '329', NULL, '136108', '136108', 'M. Nurali', NULL, 'lab_ma', 1, '136108', 'evqv5', '54ea1224ca11b673bdcd90a731ff3cea.jpg', NULL);
INSERT INTO `siswa` VALUES (3089, '328', NULL, '136085', '136085', 'Iqbal Mandala', NULL, 'lab_ma', 1, '136085', '9lof1', '', NULL);
INSERT INTO `siswa` VALUES (3090, '331', NULL, '136100', '136100', 'M Rizki Ramadhan', NULL, 'lab_ma', 1, '136100', '0euzh', 'd7959ef3b0a3614e5cbe45d4566ec238.jpg', NULL);
INSERT INTO `siswa` VALUES (3153, '331', NULL, '136138', '136138', 'Muhammad Rafli Fargali', NULL, 'lab_ma', 1, '136138', 'aa8zu', 'e902e790aedf56c0aa907835ceafb24a.jpg', NULL);
INSERT INTO `siswa` VALUES (3154, '328', NULL, '136071', '136071', 'Hamdi Tamam', NULL, 'lab_ma', 1, '136071', '87w5v', '41a0ec2578f32c73f66777c55e31595b.jpg', NULL);
INSERT INTO `siswa` VALUES (3161, '332', NULL, '136070', '136070', 'Halimah', NULL, 'lab_ma', 1, '136070', 'n8agb', '3b3b35b0d9dd3785e5db3514c9282009.jpg', NULL);
INSERT INTO `siswa` VALUES (3162, '291', NULL, '136175', '136175', 'Risma Rifatul Hasanah', NULL, 'lab_ma', 1, '136175', '335y7', 'b1fb544803b3f1dde51d0a0ca064b67e.jpg', NULL);
INSERT INTO `siswa` VALUES (3169, '325', NULL, '136037', '136037', 'Bintang Alih', NULL, 'lab_ma', 1, '136037', 'x6kjk', '7cc27a48be7b63ccf90ab79b9c3933ee.jpg', NULL);
INSERT INTO `siswa` VALUES (3172, '327', NULL, '136158', '136158', 'Nurhalimatush Solihah', NULL, 'lab_ma', 1, '136158', 'bp8xm', '79b31d2a6c1768af33af6ae2c48ade6a.jpg', NULL);
INSERT INTO `siswa` VALUES (3175, '328', NULL, '136114', '136114', 'M. Rizal Hidayat', NULL, 'lab_ma', 1, '136114', '5nqec', 'cada86f125c6b00d6cc8117c09de3764.jpg', NULL);
INSERT INTO `siswa` VALUES (3178, '326', NULL, '136216', '136216', 'Siti Chaerunnisa', NULL, 'lab_ma', 1, '136216', '8p51n', '', NULL);
INSERT INTO `siswa` VALUES (3180, '291', NULL, '136219', '136219', 'Ahmad Hafidhuddin Al Munawwar', NULL, 'lab_ma', 1, '136219', 's24jr', '5264a577c39cdad966b193f497231115.JPG', NULL);
INSERT INTO `siswa` VALUES (3182, '291', NULL, '136218', '136218', 'Taufik Hidayat', NULL, 'lab_ma', 1, '136218', '8xusx', '8896da408584010ae3c47d9ec9f99320.jpg', NULL);
INSERT INTO `siswa` VALUES (3183, '329', NULL, '136220', '136220', 'Muhammad Fahmi Amri', NULL, 'lab_ma', 1, '136220', 'paq9r', '63c015abfc599e354c7857a97b91566f.jpg', NULL);
INSERT INTO `siswa` VALUES (3627, '328', NULL, '136221', '136221', 'Bea Fitri yani S.Y. Ola', NULL, 'lab_ma', 1, '136221', 'aq128c', '', NULL);
INSERT INTO `siswa` VALUES (3632, '329', NULL, '136238', '136238', 'M Adnan Fauzi Masuk Januari 2017', NULL, 'lab_ma', 1, '136238', 'siswa', '', NULL);
INSERT INTO `siswa` VALUES (3633, '291', NULL, '136239', '136239', 'Qothrun Nada', NULL, 'lab_ma', 1, '136239', 'ronitoh', 'b8a747c82693de9755b67ded860c1061.jpg', NULL);
INSERT INTO `siswa` VALUES (3636, '329', NULL, '154107', '154107', 'M Yusuf Nurjalaludin', NULL, 'lab_ma', 1, '154107', '786gs', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for soal
-- ----------------------------
DROP TABLE IF EXISTS `soal`;
CREATE TABLE `soal` (
  `id_soal` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nomor` int(5) NOT NULL,
  `soal` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `jenis` int(1) NOT NULL,
  `pilA` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pilB` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pilC` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pilD` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pilE` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `file` text,
  `file1` text,
  `fileA` text,
  `fileB` text,
  `fileC` text,
  `fileD` text,
  `fileE` text,
  PRIMARY KEY (`id_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of soal
-- ----------------------------
BEGIN;
INSERT INTO `soal` VALUES (51, 1, 1, ':علم يعرف به كيفية قسمة التركة على مستحقيها، هذا تعريف من', 1, 'الفرائض لغةً', 'الفرائض اصطلاحا', 'علم الفرائض', 'الإرث', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (52, 1, 2, 'استمداد علم الفرائض من', 1, 'القرآن', 'القرآن و الحديث', 'الإجماع', 'القرآن و الحديث و الإجمامع', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (53, 1, 3, ' :حكم تعلم علم الفرائض، هو', 1, 'فرض عين', 'فرض كفاية', 'واجب ', 'ســنة', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (54, 1, 4, ':مِن الصحابة المشهور بعلم الفرائض', 1, 'علىي ابن أبي طالب', 'أبو هريرة', 'عثمان ابن عفان', 'ابن حجر العسقلانى ', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (55, 1, 5, 'المال الذى تركه الميت، فيسمى ب', 1, 'الإرث', 'التركة', 'الفريضة', 'الوارث', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (56, 1, 6, 'لايكون من أركان الإرث، هو', 1, 'الوارث', 'المورِّث ', 'الحق الموروث', 'الإسلام', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (57, 1, 7, 'كان المعتق وارثا وسبب نيل إرثه هي', 1, 'النسب', 'النكاح', 'المصاهرة', 'الولاء', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (58, 1, 8, 'أعتق أبو بكر الصديق  بلالا ابن ربح، فسيكون بلال', 1, 'وارثا', 'حُرّا', 'عتيقا', 'معتقا', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (59, 1, 9, 'ولكُلٍّ جعلنا الموالي ممّا ترك الوالدان و الأقربون، هذا الدليل يدل على أسباب الإرث في', 1, 'النسب', 'النكاح', 'المصاهرة', 'الولاء', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (60, 1, 10, 'النسب ينقسم إلى ثلاثة أقسام، وأما المثال من الحواشية فهو', 1, 'الابن', 'الأب', 'الأخ الشقيق', 'الابن و البنت', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (61, 1, 11, 'أن يتحقق موت الموروث ، هذا من بعض', 1, 'أسباب الإرث', 'أركان الإرث', 'شروط الإرث', 'موانع الإرث', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (62, 1, 12, 'لايرث المسلم الكافر ولا يرث الكافر المسلم، هذا الدليل يدل على', 1, 'أسباب الإرث', 'أركان الإرث', 'شروط الإرث', 'موانع الإرث', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (63, 1, 13, 'هم الذين ينالون نصيبا من الإرث', 1, 'الوارث', 'الورثة', 'المعتق', 'الكلالة', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (64, 1, 14, 'عدد الورثة من الرجال خمسة عشر نفرا، وأما الوارثات من النساء عددهنَّ', 1, 'عشرة أنفار ', 'أحد عشر نفرا ', 'إثنى عشر نفرا ', 'ثلاثة عشر نفرا ', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (65, 1, 15, 'الذين ينالون نصيبا من الإرث بغير تقدير خاص، فيقال لهم', 1, 'أصحاب الفروض', 'أصحاب التعصيب', 'أصحاب الورثة', 'أصحاب النّار ', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (66, 1, 16, 'الورثة من الرجال الذين يرثون بالفروض تارة وبالتعصيب تارة أخرى، هما', 1, 'ابن و ابن ابن', 'أب و ابن', 'أب و جدّ', 'أخ شقيق و أخ لأب ', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (67, 1, 17, 'عدد الفروض المقدرة فى القرآن الكريم', 1, 'ستة', 'سبعة ', 'ثمانية', 'تسعة ', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (68, 1, 18, 'الورثة من الرجال الذين ينالون أو يرثون بالفروض', 1, 'نفرٌ واحدٌ', 'نفران اثنان', 'ثلاثة أنفار', 'أربعة أنفار', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (69, 1, 19, 'عدد أصحاب النصف من الرجال', 1, 'واحد', 'ثلاثة', 'أربعة', 'خمسة', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (70, 1, 20, 'أن ينال الزوج ١/٤ إن كان للميت فرع، وأما الزوجة إن لم يكن للميت فرع فتنال', 1, '43497', '43556', '43678', '43647', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (71, 1, 21, 'أن تنال الأختان للأم الثلثَ إن لم يكن للميت', 1, 'فرع وارث ', 'أصل', 'زوج', 'فرع و أصل', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (72, 1, 22, 'أن تنال البنت الثلثَين  إن كانتا اثنتين ولم يكن لها معصِّبٌ، المراد من الكلمة المصتطرة', 1, 'أب', 'ابن', 'أخ شقيق', 'عمّ شقيق', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (73, 1, 23, 'مقدار الإرث للأخت للأب .........  أحوال؛', 1, '3', '4', '5', '6', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (74, 1, 24, 'مات أحد وترك الزوجة و البنت و بنت الابن و الأم، فمقدار بنت الابن هو', 1, '43497', '43526', '43525', '43617', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (75, 1, 25, 'الأب ينال السدس و العصبة معا، إذا تَوارثَ مع', 1, 'الابن و البنت', 'ابن الابن و بنت الابن', 'الابن و ابن الابن', 'البنت و بنت الابن', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (76, 1, 26, 'قرابة الرجل لأبيه و ابنه', 1, 'العصبة', 'الفريضة', 'القرابة', 'التعصيب', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (77, 1, 27, 'صارت الأنثى عاصبة باجتماعها مع الأخرى', 1, 'العصبة بالنفس', 'العصبة بالغير', 'العصبة مع الغير', 'العصبة بالمرءة', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (78, 1, 28, 'للذكر مثل حظ الأنثيين، هذا دليل على طريق تقسيم الإرث فى', 1, 'العصبة بالنفس', 'العصبة بالغير', 'العصبة مع الغير', 'الفروض المقدرة ', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (79, 1, 29, 'من بعض ذكر معصِّب فى العصبة بالغير', 1, 'أب و جدّ', 'أب و ابن', 'ابن و أخ شقيق', 'أخ شقيق و عمّ شقيق', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (80, 1, 30, 'كانت الأخت الشقيقة عاصبةً مع الغير إذا اجتمعت مع', 1, 'ألأم', 'البنت', 'الزوجة', 'الأخ الشقيق', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (81, 1, 31, 'منع وارث من الإرث سواء كان بجميعه أو ببعضه، هذا التعريف من', 1, 'الفرض', 'العصبة', 'الحجب', 'العتق', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (82, 1, 32, 'الوارثة المحجوبة بالأم من الفوقية', 1, 'الجدة من جهة الأم و الجد من جهة الأم', 'الجدة من جهة الأب و الجد من جهة الأب', 'الجدة من جهة الأم و الجدة من جهة الأب', 'الجد من جهة الأب و الجد من جهة الأم', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (83, 1, 33, 'منع وارث من الإرث بوجود وارث آخر، هذا معنى من', 1, 'الحجب بالنقصان', 'الحجب بالانتقال', 'الحجب بالحرمان', 'الحجب بالصفة', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (84, 1, 34, 'أن تنال الأم الثلث إذا عدم الفرع، وتنال السدس إذا وجد الفرع، هذا المثال من', 1, 'الحجب بالإسقاط', 'الحجب بالانتقال', 'الحجب بالحرمان', 'الحجب بالصفة ', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (85, 1, 35, 'إذا اجتمع الأب و الجد الحقيقي و الزوج، فمقدار الجد الحقيقي', 1, '43617', 'عصبةٌ', '١/٦ و عصبةٌ', 'محجوبٌ', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (86, 1, 36, 'من الورثة الذين لا يَحجبون ولا يُحجبون', 1, 'الأب والأم', 'الابن و البنت', 'الزوح و الزوجة', 'الجد و الجدة', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (87, 1, 37, 'الورثة الذين  لا يسقطون من الإرث فى حال من الأحوال', 1, 'نفران', 'ثلاثة أنفار', 'أربعة أنفار', 'خمسة أنفار', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (88, 1, 38, 'أول وقوع مسألة العول فى عهد', 1, 'أبي بكر الصديق', 'عمر ابن الخطاب', 'عثمان ابن عفان', 'عليّ ابن أبي طالب ', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (89, 1, 39, 'يعول أصل المسألة من الستة إلى', 1, '٧، ٨، ٩، ١٠', '٦، ٧، ٨، ٩', '٦، ٧، ٨، ٩، ١٠', '٧، ٨، ٩، ١٠، ١١', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (90, 1, 40, 'إذا اجتمعت الزوج و الأم و الأخت الشثيثة،  فوقعت مسألة العول من الستة إلى', 1, '6', '7', '8', '9', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (91, 1, 41, 'لم تقع مسألة العول فى اثنتي عشر إلاّ فى', 1, 'حالين', 'ثلاثة أحوال', 'أربعة أحوال', 'خمسة أحوال ', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (92, 1, 42, 'إذا اجتمعت الزوج و البنت و الأم و الجد الحقيقي،  فوقعت مسألة العول من الستة إلى', 1, '13', '14', '15', '16', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (93, 1, 43, 'زيادة على سهم الوارث و نقص فى نصيبه، هذا التعريف من', 1, 'العول', 'الرد', 'الحجب', 'ذووا الأرحام ', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (94, 1, 44, 'من بعض أصناف الرد هن', 1, 'بنت وبنت ابن و أم', 'بنت و بنت ابن و بنت بنت', 'بنت و أم و زوجة', 'زوجة و أم و جدة ', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (95, 1, 45, 'أصناف الرد من الرجل فقط نفر واحد، هو', 1, 'ابن', 'أخ شقيق', 'أخ لأب', 'أخ لأم', '', 'D', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (96, 1, 46, 'إذا اجتمعت الأم مع البنت و الزوجة فوقعت المسألة فى', 1, 'التعصيب', 'العول', 'الردّ', 'ذوي الأرحام', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (97, 1, 47, 'إذا اجتمعت الأم مع البنت و الزوجة و الأخت الشقيقة، فترث الأم', 1, 'ثلث', 'سدس', 'ثلث الباقى', 'محجوبة', '', 'B', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (98, 1, 48, 'توفيت المسلمة و لها زوج و أم و ابن و ثلاث بنات و ابن ابن و أخ شقيق، و الأموال بمقدار ١٢٠٠٠٠٠٠٠ روبيةً. فنصيب الزوج، هو', 1, '٦٠٠٠٠٠٠٠ روبية', '٦٠٠٠٠٠٠ روبية', '٣٠٠٠٠٠٠٠ روبية', '١٥٠٠٠٠٠٠ روبية', '', 'C', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (99, 1, 49, 'توفيت المرأة و تركت الزوج و الأم و الابن و البنتين و ابن الابن و الأخ الشقيق، و الأموال بمقدار ٤٨٠٠٠٠٠٠ روبيةً ، فنصيب كل بنت', 1, '٧٠٠٠٠٠٠ روبية', '١٤٠٠٠٠٠٠ روبية', '١٦٠٠٠٠٠٠ روبية', '٢٤٠٠٠٠٠٠ روبية', '', 'A', '', '', '', '', '', '', '');
INSERT INTO `soal` VALUES (100, 1, 50, 'توفي الكريمة و ترك الزوج و الأختين الشقيقتين، و الأخ للأم و العم الشقيق و الأموال بمقدار : ٢٤٠٠٠٠٠٠٠ روبية . فنصيب الأخ للأم', 1, '٨٠٠٠٠٠٠٠ روبية', '٦٠٠٠٠٠٠٠ روبية', '٣٠٠٠٠٠٠٠ روبية', '١٠٠٠٠٠٠٠ روبية', '', 'C', '', '', '', '', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(6) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `masa_berlaku` time NOT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of token
-- ----------------------------
BEGIN;
INSERT INTO `token` VALUES (1, 'MAPZHL', '2019-03-20 20:17:45', '00:15:00');
COMMIT;

-- ----------------------------
-- Table structure for ujian
-- ----------------------------
DROP TABLE IF EXISTS `ujian`;
CREATE TABLE `ujian` (
  `id_ujian` int(5) NOT NULL AUTO_INCREMENT,
  `id_pk` varchar(10) DEFAULT NULL,
  `id_guru` int(5) NOT NULL,
  `id_mapel` int(5) NOT NULL,
  `nama` varchar(100) CHARACTER SET utf32 COLLATE utf32_bin NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `lama_ujian` int(5) NOT NULL,
  `tgl_ujian` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `waktu_ujian` time NOT NULL,
  `selesai_ujian` time NOT NULL,
  `level` varchar(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `acak` int(1) NOT NULL,
  `token` int(1) NOT NULL,
  `status` int(3) NOT NULL,
  `hasil` int(2) NOT NULL,
  `proktor` varchar(30) NOT NULL,
  `pengawas` varchar(30) NOT NULL,
  `nip_pengawas` varchar(30) NOT NULL,
  `catatan` text NOT NULL,
  PRIMARY KEY (`id_ujian`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ujian
-- ----------------------------
BEGIN;
INSERT INTO `ujian` VALUES (5, NULL, 51, 1, 'Tahriri Niahie 2019 - Tajwid', 50, 0, 100, 0, 25, 0, 90, '2019-04-08 07:00:00', '2019-04-08 22:00:00', '07:00:00', '00:00:00', '', 'a:9:{i:0;s:3:\"291\";i:1;s:3:\"325\";i:2;s:3:\"326\";i:3;s:3:\"327\";i:4;s:3:\"328\";i:5;s:3:\"329\";i:6;s:3:\"330\";i:7;s:3:\"331\";i:8;s:3:\"332\";}', 1, 0, 1, 0, '', '', '', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
